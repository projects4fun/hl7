#### A HL7v2 message processing framework

This library is a HL7 processing framework providing support for interacting
with messages from files, strings, and streams. 


#### Objectives

This project attempts to work with HL7 data, all of its versions, with all of
its intricacies. There are frameworks out there in Java that have implemented 
support for HL7 and are more feature complete than this framework. Many of these
frameworks implement HL7 profiles which govern how some messages should work.
This core framework does not concern itself with profiles and aims more for
general support with HL7 messages and their version not caring too much if a
ADT properly provides the segments necessary for a A01 vs an A08. A separate
module may be implemented with this support but as of this writing this support
is out of scope. 

#### Examples

##### Changing the version number of a HL7 message    
    
    String resource = "sample-resource-one.txt";
    // load the resource into a stream
    try (InputStream stream = getClass().getResourceAsStream(resource)) {
    
        // use one of the overloaded from() methods
        GenericMessage message = Parse.from(stream);
        Slicer slicer = message.getSlicer();

        // find the segment you wish to work with
        Optional<Segment> optional = slicer.findSegment("MSH");

        // ensure that the segment was found
        Assertions.assertTrue(optional.isPresent());

        // set the value you wish to see
        MSH msh = (MSH) optional.get();
        msh.setVersionId("2.5.1");

        // test the results 
        // use .replace("\r", "\n") when outputting to unix terminals
        System.out.println(message.toString());
    } catch (Exception e) {
        Assertions.fail(e);
    }