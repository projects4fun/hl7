package org.sparksource.hl7.fields;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.builder.MessageBuilder;
import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.parser.Slicer;
import org.sparksource.hl7.segments.Segment;

import java.io.InputStream;
import java.util.List;

/**
 * <p>Attempts to handle HL7 protected character cases.</p>
 *
 * @author Juan Garcia
 * @since 2019-04-24
 */
class ProtectedCharacterTest {

    @Test
    @DisplayName("\\T\\ character sequence is unescaped to &")
    void ampersand_character_sequence_is_unescaped_on_consumption() {
        String resource = "nist-test-lab-app.txt";
        try (InputStream stream = getClass().getResourceAsStream(resource)) {
            GenericMessage message = Parse.from(stream);

            Slicer slicer = message.getSlicer();
            List<Segment> segmentList = slicer.findSegmentList("OBR");

            Assertions.assertFalse(segmentList.isEmpty());

            Segment segment = segmentList.get(1);
            String field = segment.getSegmentFields().get(4);

            CE ce = new CE(message).build(field);

            Assertions.assertEquals("Hepatitis C virus RNA [Units/volume] (viral load) in Serum or Plasma by Probe & target amplification method", ce.getText());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("& is converted to a \\T\\ on escape")
    void ampersand_character_is_escaped_on_consumption() {
        GenericMessage message = MessageBuilder.newInstance();

        XAD xad = new XAD(message);
        xad.setStreetAddress("One way & Two way street");
        xad.setCity("Kansas City");
        xad.setCountry("US");

        Assertions.assertEquals("One way \\T\\ Two way street^^Kansas City^^^US", xad.toString());
    }
}
