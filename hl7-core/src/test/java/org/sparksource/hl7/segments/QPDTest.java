package org.sparksource.hl7.segments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.builder.MessageBuilder;

/**
 * @author Juan Garcia
 * @since 2018-01-09
 */
class QPDTest {

    @Test
    @DisplayName("QPD segment can be populated")
    void qpd_segment_can_be_populated() {
        GenericMessage genericMessage = MessageBuilder.newInstance();

        QPD qpd = new QPD(genericMessage);
        qpd.setFieldValue(QPD.FieldName.SEX, "F");

        Assertions.assertEquals("QPD|||||||F", qpd.toString());
    }
}
