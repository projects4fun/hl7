package org.sparksource.hl7.segments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.builder.MessageBuilder;

/**
 * @author Juan Garcia
 * @since 2017-08-30
 */
class PIDTest {

    @Test
    @DisplayName("A new PID segment leads with PID")
    void new_pid_segment_leads_with_pid() {
        PID pid = new PID(new GenericMessage());

        Assertions.assertTrue(pid.toString().startsWith("PID"));
    }

    @Test
    @DisplayName("PID segment does not create unnecessary segments")
    void pid_segment_does_not_create_extra_segments() {
        try {
            PID pid = new PID(MessageBuilder.newInstance()).build("PID|3");
            Assertions.assertEquals(5, pid.toString().length());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
