package org.sparksource.hl7.segments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.parser.Slicer;

import java.util.List;

/**
 * @author Juan Garcia
 * @since 2019-04-22
 */
class AL1Test {

    @Test
    @DisplayName("GenericMessage can read AL1 segments")
    void generic_message_can_read_al1_segments() {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-message-one.txt"));

            Slicer slicer = message.getSlicer();
            List<Segment> segments = slicer.findSegmentList("AL1");
            Assertions.assertEquals(2, segments.size());

            for (Segment segment : segments) {
                Assertions.assertTrue(segment instanceof AL1);
            }

            Assertions.assertEquals("AL1|1||^Penicillin||Produces hives",
                segments.get(0).toString());
            Assertions.assertEquals("AL1|2||^Cat dander|Respiratory distress",
                segments.get(1).toString());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
