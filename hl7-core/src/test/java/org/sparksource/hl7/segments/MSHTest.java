package org.sparksource.hl7.segments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;

/**
 * @author Juan Garcia
 * @since 2017-08-15
 */
class MSHTest {

    private static MSH msh;

    @BeforeAll
    static void setup() {
        try {
            final String mshLine = "MSH|^~\\&|PWX|PWX|HUB|HUB|20170701232108||ORU^R01|Q0T0||2.3||||||8859/1";
            msh = MSH.parse(mshLine);
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("A new MSH segment leads with MSH")
    void new_msh_segment_leads_with_msh() {
        MSH msh = new MSH(new GenericMessage());

        Assertions.assertTrue(msh.toString().startsWith("MSH"));
    }

    @Test
    @DisplayName("field separator is a pipe")
    void field_separator_is_a_pipe() {
        Assertions.assertEquals("|", msh.getFieldDelimiter());
    }

    @Test
    @DisplayName("message type is ORU^R01")
    void message_type_is_oru_r01() {
        Assertions.assertEquals("ORU^R01", msh.getMessageType());
    }

    @Test
    @DisplayName("message control id is Q0T0")
    void message_control_id_is_q0t0() {
        Assertions.assertEquals("Q0T0", msh.getMessageControlId());
    }

    @Test
    @DisplayName("defining control characters places characters in correct location")
    void defined_control_characters_are_in_correct_location() {
        MSH msh = new MSH(new GenericMessage());
        msh.setEncodingCharacters("^~\\&");
        Assertions.assertEquals("MSH|^~\\&", msh.toString());
    }
}
