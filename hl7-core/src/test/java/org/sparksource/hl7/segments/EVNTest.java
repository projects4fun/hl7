package org.sparksource.hl7.segments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.builder.MessageBuilder;
import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.parser.Slicer;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Juan Garcia
 * @since 2017-09-08
 */
class EVNTest {

    @Test
    @DisplayName("A new EVN segment leads with EVN")
    void evn_segment_leads_with_evn() {
        EVN evn = new EVN(MessageBuilder.newInstance());
        assertTrue(evn.toString().startsWith("EVN"));
    }

    @Test
    @DisplayName("Built EVN segment creates correct field values")
    void built_evn_segment_creates_correct_field_values() {
        try {
            EVN evn = new EVN(MessageBuilder.newInstance()).build("EVN|A01|198808181123");

            assertEquals("EVN|A01|198808181123", evn.toString());

            Optional<String> eventOptional = evn.getFieldValue(EVN.FieldName.EVENT_TYPE_CODE);
            Assertions.assertTrue(eventOptional.isPresent());
            Assertions.assertEquals("A01", eventOptional.get());

            Optional<String> dateTimeOptional = evn.getFieldValue(EVN.FieldName.RECORDED_DATETIME);
            Assertions.assertTrue(dateTimeOptional.isPresent());
            Assertions.assertEquals("198808181123", dateTimeOptional.get());
        } catch (Exception e) {
            Assertions.fail(e);
        }

    }

    @Test
    @DisplayName("GenericMessage can read EVN segments")
    void generic_message_can_read_evn_segments() {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-message-one.txt"));

            Slicer slicer = message.getSlicer();
            List<Segment> segments = slicer.findSegmentList("EVN");
            Assertions.assertEquals(1, segments.size());

            for (Segment segment : segments) {
                Assertions.assertTrue(segment instanceof EVN);
            }

            assertEquals("EVN|A01|198808181123",
                segments.get(0).toString());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
