package org.sparksource.hl7.segments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.parser.Slicer;

import java.util.List;

/**
 * @author Juan Garcia
 * @since 2019-04-22
 */
class NK1Test {

    @Test
    @DisplayName("GenericMessage can read NK1 segment")
    void generic_message_can_read_nk1_segment() {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-message-one.txt"));

            Slicer slicer = message.getSlicer();
            List<Segment> segments = slicer.findSegmentList("NK1");
            Assertions.assertEquals(1, segments.size());

            for (Segment segment : segments) {
                Assertions.assertTrue(segment instanceof NK1);
            }

            Assertions.assertEquals("NK1|1|JONES^BARBARA^K|WIFE||||||NK",
                segments.get(0).toString());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
