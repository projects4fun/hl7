package org.sparksource.hl7;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.segments.EVN;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Juan Garcia
 * @since 2017-08-15
 */
class SegmentTest {

    @Test
    @DisplayName("internal list grows to fill fields")
    void internal_list_grows_to_fill_fields() {
        EVN evn = new EVN(new GenericMessage());

        evn.setFieldValue(10, "SPOCK_TEST");

        assertEquals(11, evn.getSegmentFields().size());
    }
}
