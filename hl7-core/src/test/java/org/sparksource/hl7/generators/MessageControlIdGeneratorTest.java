package org.sparksource.hl7.generators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-01-09
 */
class MessageControlIdGeneratorTest {

    @Test
    @DisplayName("IdGenerator next value generates different values")
    void id_request_provides_new_value() {
        Assertions.assertNotEquals(MessageControlIdGenerator.next(), MessageControlIdGenerator.next());
    }
}
