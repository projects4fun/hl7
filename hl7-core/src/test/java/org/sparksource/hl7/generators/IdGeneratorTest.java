package org.sparksource.hl7.generators;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Juan Garcia
 * @since 2017-11-14
 */
class IdGeneratorTest {

    @BeforeAll
    static void setupSpec() {
        try {
            Path path = Paths.get(IdGenerator.ID_STORE);
            Files.deleteIfExists(path);
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("A id request provides a BigInteger value")
    void id_request_provides_new_value() {
        assertTrue(IdGenerator.next() != 0);
    }
}
