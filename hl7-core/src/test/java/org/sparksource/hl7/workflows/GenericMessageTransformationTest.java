package org.sparksource.hl7.workflows;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;
import org.sparksource.hl7.parser.InvalidFieldQueryException;
import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.parser.Slicer;
import org.sparksource.hl7.segments.MSH;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Juan Garcia
 * @since 2017-10-09
 */
class GenericMessageTransformationTest {

    @Test
    @DisplayName("A GenericMessage with invalid version can be transformed")
    void message_with_invalid_version_can_be_transformed() throws IllegalMessageFormat {
        String resource = "sample-resource-one.txt";
        try (InputStream stream = getClass().getResourceAsStream(resource)) {
            GenericMessage message = Parse.from(stream);

            Slicer slicer = new Slicer(message);
            MSH msh = (MSH) slicer.findSegment("MSH").get();
            msh.setVersionId("2.5");

            assertEquals("f6b868502f229d8b79fd507b9253f86cbc8f4ac3",
                DigestUtils.sha1Hex(message.toString()));
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("A GenericMessage with default setMessageControlId has control id defined")
    void message_can_have_message_control_id_defined() throws IllegalMessageFormat {
        final String resource = "sample-resource-one.txt";
        try (InputStream stream = getClass().getResourceAsStream(resource)) {

            GenericMessage message = Parse.from(stream);

            Slicer slicer = new Slicer(message);
            MSH msh = (MSH) slicer.findSegment("MSH").get();

            final String defaultControlId = slicer.findField("MSH-10").get();

            msh.setMessageControlId();
            final String alteredControlId = slicer.findField("MSH-10").get();

            assertNotEquals(defaultControlId, alteredControlId);
        } catch (IOException | InvalidFieldQueryException e) {
            fail(e);
        }
    }
}
