package org.sparksource.hl7.workflows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.builder.MessageBuilder;
import org.sparksource.hl7.fields.XAD;
import org.sparksource.hl7.fields.XPN;
import org.sparksource.hl7.generators.MessageControlIdGenerator;
import org.sparksource.hl7.parser.Slicer;
import org.sparksource.hl7.segments.MSH;
import org.sparksource.hl7.segments.QPD;
import org.sparksource.hl7.segments.RCP;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Juan Garcia
 * @since 2018-01-09
 */
class ImmunizationsQPDTest {

    /**
     * <p>Proof of concept QPD message creation for spaces immunizations
     * query.</p>
     */
    @Test
    @DisplayName("QPD segment can be built using QPD class")
    void qpd_segment_can_be_build_using_qpd_class() {
        GenericMessage message = MessageBuilder.newInstance();
        Slicer slicer = message.getSlicer();

        OffsetDateTime dateTime = OffsetDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

        MSH msh = (MSH) slicer.findSegment("MSH").get();
        msh.setSendingApplication("LIFE_MD");
        msh.setSendingFacility("110043");
        msh.setReceivingApplication("CRISP");
        msh.setReceivingFacility("CRISP");
        msh.setDateTime(formatter.format(dateTime));
        msh.setMessageType("QBP^Q11^QBP_Q11");
        msh.setProcessingId("P");
        msh.setVersionId("2.5.1");
        msh.setAcceptAckType("NE");
        msh.setApplicationAckType("AL");
        msh.setMessageControlId(MessageControlIdGenerator.next());
        msh.setFieldValue(MSH.FieldName.MESSAGE_PROFILE_IDENTIFIER, "Z34^CDCPHINVS");

        QPD qpd = new QPD(message)
            .setFieldValue(QPD.FieldName.MESSAGE_QUERY_NAME, "Z34^Request Complete Immunization History^CDCPHINVS")
            .setFieldValue(QPD.FieldName.QUERY_TAG, "XDOC-1892146");

        XAD xad = new XAD(message);
        xad.setStreetAddress("799 NE Micheal Dr");
        xad.setCity("Kansas City");
        xad.setState("MO");
        xad.setPostalCode("1122");

        qpd.setFieldValue(QPD.FieldName.ADDRESS, xad.toString());

        XPN xpn = new XPN(message);
        xpn.setFamilyName("NOT");
        xpn.setGivenName("REAL");

        qpd.setFieldValue(QPD.FieldName.NAME, xpn.toString());
        qpd.setFieldValue(QPD.FieldName.DATE_OF_BIRTH, "20011213");
        qpd.setFieldValue(QPD.FieldName.SEX, "F");

        // message.getSegments().add(qpd);
        message.getSegments().add(qpd);

        RCP rcp = new RCP(message);
        rcp.setFieldValue(RCP.FieldName.QUERY_PRIORITY, "I");
        rcp.setFieldValue(RCP.FieldName.QUANTITY_LIMITED_REQUEST, "5^RD&Records&HL70126");
        rcp.setFieldValue(RCP.FieldName.RESPONSE_MODALITY, "R^real-time^HL70394");

        message.getSegments().add(rcp);
    }
}
