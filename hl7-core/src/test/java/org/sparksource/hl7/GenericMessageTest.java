package org.sparksource.hl7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.builder.MessageBuilder;
import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.segments.PID;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Juan Garcia
 * @since 2017-08-30
 */
class GenericMessageTest {

    @Test
    @DisplayName("Unknown segment type is not discarded")
    void unknown_segment_type_is_not_discarded() throws IllegalMessageFormat {
        try (InputStream resource = getClass().getResourceAsStream("sample-resource-one.txt")) {
            GenericMessage message = Parse.from(resource);

            assertEquals(5, message.getSegments().size());
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("message can be generated from File")
    void message_can_be_generated_from_file() throws IllegalMessageFormat {
        try {
            File file = new File("resources/sample-resource-files-one.txt");

            GenericMessage message = Parse.from(file);

            assertEquals(9, message.getSegments().size());
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("Created message can be written out to a Path location")
    void created_message_can_be_written_out_to_path_location() {
        GenericMessage m = MessageBuilder.newInstance();
        PID pid = new PID(m);
        pid.setPatientName("LEEROY^JENKINS");
        m.addSegment(pid);

        try {
            File file = new File("target/" + UUID.randomUUID().toString() + ".hl7");
            m.toFile(file.toPath());
            Assertions.assertTrue(file.exists());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("Created message can be written out to a File location")
    void created_message_can_be_written_out_to_file_location() {
        GenericMessage m = MessageBuilder.newInstance();
        PID pid = new PID(m);
        pid.setPatientName("LEEROY^JENKINS");
        m.addSegment(pid);

        try {
            File file = new File("target/" + UUID.randomUUID().toString() + ".hl7");
            m.toFile(file);
            Assertions.assertTrue(file.exists());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("Created message can be written out to a String location")
    void created_message_can_be_written_out_to_string_location() {
        GenericMessage m = MessageBuilder.newInstance();
        PID pid = new PID(m);
        pid.setPatientName("LEEROY^JENKINS");
        m.addSegment(pid);

        try {
            final String location = "target/" + UUID.randomUUID().toString() + ".hl7";
            File file = new File(location);
            m.toFile(location);
            Assertions.assertTrue(file.exists());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
