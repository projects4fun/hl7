package org.sparksource.hl7.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;
import org.sparksource.hl7.segments.MSH;
import org.sparksource.hl7.segments.PID;
import org.sparksource.hl7.segments.Segment;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Juan Garcia
 * @since 2017-09-10
 */
class SlicerTest {

    @Test
    @DisplayName("PID can be extracted from GenericMessage")
    void pid_can_be_extracted_from_message() throws IllegalMessageFormat {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-resource-one.txt"));

            Slicer slicer = new Slicer(message);
            Optional<Segment> optional = slicer.findSegment("PID");

            Assertions.assertTrue(optional.isPresent());
            Segment segment = optional.get();

            assertTrue(segment instanceof PID);
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("MSH can be extracted from GenericMessage")
    void msh_can_be_extracted_from_message() throws IllegalMessageFormat {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-resource-one.txt"));

            Slicer slicer = new Slicer(message);
            Optional<Segment> optional = slicer.findSegment("MSH");

            Assertions.assertTrue(optional.isPresent());
            Segment segment = optional.get();

            assertTrue(segment instanceof MSH);
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("find field can extract message type")
    void find_field_can_extract_message_type() throws IllegalMessageFormat {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-resource-one.txt"));

            Slicer slicer = new Slicer(message);
            Optional<String> optional = slicer.findField("MSH-9");

            Assertions.assertTrue(optional.isPresent());
            String field = optional.get();

            assertEquals("ORU^R01", field);
        } catch (IOException | InvalidFieldQueryException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("README example usage is valid")
    void readme_example_usage_is_valid() {
        String resource = "sample-resource-one.txt";
        try (InputStream stream = getClass().getResourceAsStream(resource)) {
            GenericMessage message = Parse.from(stream);

            Slicer slicer = message.getSlicer();
            Optional<Segment> optional = slicer.findSegment("MSH");

            Assertions.assertTrue(optional.isPresent());

            MSH msh = (MSH) optional.get();
            msh.setVersionId("2.5.1");

            System.out.println(message.toString().replace("\r", "\n"));
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("find field can extract external patient id")
    void find_field_can_extract_external_patient_id() throws IllegalMessageFormat {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-resource-one.txt"));

            Slicer slicer = new Slicer(message);
            Optional<String> optional = slicer.findField("PID-3");

            Assertions.assertTrue(optional.isPresent());
            String field = optional.get();

            assertEquals("1111^^^CHWN_CA_ACCT_MRN", field);
        } catch (IOException | InvalidFieldQueryException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("find field does extract ORC correctly")
    void find_field_does_extract_orc_correctly() throws IllegalMessageFormat {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-resource-one.txt"));

            Slicer slicer = new Slicer(message);
            Optional<String> optional = slicer.findField("ORC-3");

            Assertions.assertTrue(optional.isPresent());
            String field = optional.get();

            assertEquals("32630-EN789323YClinicalPDFReport10^HNAM_CEREF~599326653^HNAM_EVENTID", field);
        } catch (IOException | InvalidFieldQueryException e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("GenericMessage get slicer method can extract ORC correctly")
    void message_get_slicer_can_extract_orc_correctly() {
        try {
            GenericMessage message = Parse.from(getClass()
                .getResourceAsStream("sample-resource-one.txt"));

            Slicer slicer = message.getSlicer();
            Optional<String> optional = slicer.findField("ORC-3");

            Assertions.assertTrue(optional.isPresent());
            String field = optional.get();

            assertEquals("32630-EN789323YClinicalPDFReport10^HNAM_CEREF~599326653^HNAM_EVENTID", field);
        } catch (Exception e) {
            fail(e);
        }
    }
}
