package org.sparksource.hl7;

/**
 * @author Juan Garcia
 * @since 2016-04-11
 */
public class IllegalMessageFormat extends Exception {

    public IllegalMessageFormat(String message) {
        super(message);
    }
}
