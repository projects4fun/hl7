package org.sparksource.hl7;

import java.io.*;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * <p>An implementation to send a {@link Message} over MLLP. MLLP is a {@link java.net.Socket}
 * connection with three delimiters to wrap a message. The delimiters are 0x0B,
 * 0x1C, and OxOD and hardcoded as constants. When sending a {@link Message}
 * over the wire the standard states that we must use \r so we construct
 * messages using \n and right before sending replace \n with \r.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-28
 */
public class MllpStream {

    final private static Logger logger = Logger.getLogger(MllpStream.class.getName());

    final private static byte MLLP_START = 0x0b;
    final private static byte MLLP_END1 = 0x1c;
    final private static byte MLLP_END2 = 0x0d;

    /**
     * <p>Writes content to a remote site. This method is specific to MLLP as we
     * transform platform specific characters here rather than deal with them
     * while constructing a {@link Message}.</p>
     *
     * @param outputStream the {@link OutputStream} that we are writing to
     * @param string       the {@link String} that we want to send to a remote site
     * @throws IOException if the steam is closed
     */
    public static void write(final OutputStream outputStream, final String string) throws IOException {
        logger.finer(() -> String.format("we are going to send:\n%s", string));

        outputStream.write(MLLP_START);
        outputStream.write(string.replace("\n", "\r").getBytes(UTF_8));
        outputStream.write(MLLP_END1);
        outputStream.write(MLLP_END2);
        outputStream.flush();
    }

    /**
     * <p>Reads the response from a remote site the use of HL7 requirements of
     * delimiters allows us to reliably read a complete HL7 message. Note that
     * this read does not close a {@link InputStream} allowing us to continue to
     * use a given {@link java.net.Socket}.</p>
     *
     * @param inputStream the {@link InputStream} of the remote site
     * @return a {@link String} of the data that was in the {@link InputStream}
     * @throws IOException if the steam is closed
     */
    public static String read(final InputStream inputStream) throws IOException {
        logger.finer("reading HL7 message from client");

        final StringBuilder stringBuilder = new StringBuilder();

        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String tempLine;
        while ((tempLine = bufferedReader.readLine()) != null) {
            if (tempLine.contains(Character.toString((char) MLLP_END2))
                || tempLine.endsWith(Character.toString((char) MLLP_END1))) {

                stringBuilder.append(tempLine).append("\n");
                break;
            }
            stringBuilder.append(tempLine).append("\n");
        }

        logger.finest(() -> String.format("HL7 response before escape sequences are replaced: %s",
            stringBuilder.toString()));

        String response = stringBuilder.toString();
        response = response.replace(Character.toString((char) MLLP_START), "");
        response = response.replace(Character.toString((char) MLLP_END1), "");
        response = response.replace(Character.toString((char) MLLP_END2), "");

        return response;
    }
}
