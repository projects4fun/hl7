package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;

import java.util.logging.Logger;

/**
 * @author Juan Garcia
 * @since 2016-04-14
 */
public class HD extends Field {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private enum FieldName {
        NAMESPACE_ID(0), UNIVERSAL_ID(1), UNIVERSAL_ID_TYPE(2);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public HD(final GenericMessage message) {
        super(message);
    }

    @Override
    public Field build(final String field) {
        HD hd = new HD(getMessage());

        if (field == null || field.trim().equals("")) {
            logger.finest("Returning empty HD object for empty content.");
            return hd;
        }

        final String[] fields = field.split(escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            hd.setFieldItem(i, fields[i]);
        }

        return hd;
    }

    public String getNamespaceId() {
        return getFieldItem(FieldName.NAMESPACE_ID.value);
    }

    public void setNamespaceId(final String namespaceId) {
        setFieldItem(FieldName.NAMESPACE_ID.getValue(), namespaceId);
    }

    public String getUniversalId() {
        return getFieldItem(FieldName.UNIVERSAL_ID.value);
    }

    public void setUniversalId(final String universalId) {
        setFieldItem(FieldName.UNIVERSAL_ID.getValue(), universalId);
    }

    public String getUniversalIdType() {
        return getFieldItem(FieldName.UNIVERSAL_ID_TYPE.value);
    }

    public void setUniversalIdType(final String universalIdType) {
        setFieldItem(FieldName.UNIVERSAL_ID_TYPE.getValue(), universalIdType);
    }
}
