package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;

import java.util.logging.Logger;

/**
 * <p>HL7 XAD extended address parsing</p>
 *
 * @author Juan Garcia
 * @since 2016-08-15
 */
public class XAD extends Field {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    public enum FieldName {
        STREET_ADDRESS(0), OTHER_DESIGNATION(1), CITY(2), STATE_OR_PROVINCE(3),
        ZIP_OR_POSTAL_CODE(4), COUNTRY(5), ADDRESS_TYPE(6),
        OTHER_GEOGRAPHIC_DESIGNATION(7), COUNTY(8),
        CENSUS_TRACT(9), ADDRESS_REPRESENTATION_CODE(10);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public XAD(final GenericMessage message) {
        super(message);
    }

    @Override
    public XAD build(final String field) {
        XAD xad = new XAD(getMessage());

        if (field == null || field.trim().equals("")) {
            logger.finest("Returning empty XAD object for empty content.");
            return xad;
        }

        final String[] fields = field.split(escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            xad.setFieldItem(i, fields[i]);
        }

        return xad;
    }

    public String getStreetAddress() {
        return getFieldItem(FieldName.STREET_ADDRESS.value);
    }

    public void setStreetAddress(final String streetAddress) {
        setFieldItem(FieldName.STREET_ADDRESS.getValue(), streetAddress);
    }

    public String getOtherDesignation() {
        return getFieldItem(FieldName.OTHER_DESIGNATION.value);
    }

    public void setOtherDesignation(final String otherDesignation) {
        setFieldItem(FieldName.OTHER_DESIGNATION.getValue(), otherDesignation);
    }

    public String getCity() {
        return getFieldItem(FieldName.CITY.value);
    }

    public void setCity(final String city) {
        setFieldItem(FieldName.CITY.getValue(), city);
    }

    public String getState() {
        return getFieldItem(FieldName.STATE_OR_PROVINCE.value);
    }

    public void setState(final String state) {
        setFieldItem(FieldName.STATE_OR_PROVINCE.getValue(), state);
    }

    public String getPostalCode() {
        return getFieldItem(FieldName.ZIP_OR_POSTAL_CODE.value);
    }

    public void setPostalCode(final String postalCode) {
        setFieldItem(FieldName.ZIP_OR_POSTAL_CODE.getValue(), postalCode);
    }

    public String getCountry() {
        return getFieldItem(FieldName.COUNTRY.value);
    }

    public void setCountry(final String country) {
        setFieldItem(FieldName.COUNTRY.getValue(), country);
    }

    public String getAddressType() {
        return getFieldItem(FieldName.ADDRESS_TYPE.value);
    }

    public void setAddressType(final String addressType) {
        setFieldItem(FieldName.ADDRESS_TYPE.getValue(), addressType);
    }

    public String getOtherGeographicDesignation() {
        return getFieldItem(FieldName.OTHER_GEOGRAPHIC_DESIGNATION.value);
    }

    public void setOtherGeographicDesignation(final String otherGeographicDesignation) {
        setFieldItem(FieldName.OTHER_GEOGRAPHIC_DESIGNATION.getValue(), otherGeographicDesignation);
    }

    public String getCounty() {
        return getFieldItem(FieldName.COUNTY.value);
    }

    public void setCounty(final String county) {
        setFieldItem(FieldName.COUNTY.value, county);
    }

    public String getCensusTract() {
        return getFieldItem(FieldName.CENSUS_TRACT.value);
    }

    public void setCensusTract(final String censusTract) {
        setFieldItem(FieldName.CENSUS_TRACT.getValue(), censusTract);
    }

    public String getAddressRepresentationCode() {
        return getFieldItem(FieldName.ADDRESS_REPRESENTATION_CODE.value);
    }

    public void setAddressRepresentationCode(final String addressRepresentationCode) {
        setFieldItem(FieldName.ADDRESS_REPRESENTATION_CODE.getValue(), addressRepresentationCode);
    }
}
