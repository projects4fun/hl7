package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;

import java.util.logging.Logger;

/**
 * <p>An attempt of creating a API around {@link CX}. The new initialization of a {@link CX} should provide an empty
 * {@link Field} with no values filled in.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-15
 */
public class CX extends Field {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    private enum FieldName {
        ID(0),
        CHECK_DIGIT(1),
        CODE_IDENTIFIER(2),
        ASSIGNING_AUTHORITY(3),
        IDENTIFIER_TYPE_CODE(4),
        ASSIGNING_FACILITY(5),
        EFFECTIVE_DATE(6),
        EXPIRATION_DATE(7),
        ASSIGNING_JURISDICTION(8),
        ASSIGNING_AGENCY(9);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public CX(final GenericMessage genericMessage) {
        super(genericMessage);
    }

    @Override
    public CX build(final String field) {
        CX cx = new CX(getMessage());
        if (field == null || field.trim().equals("")) {
            logger.finest("Returning empty CX object for empty content.");
            return cx;
        }

        final String[] fields = field.split(escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            cx.setFieldItem(i, fields[i]);
        }

        return cx;
    }

    public String getId() {
        return getFieldItem(FieldName.ID.value);
    }

    public void setId(String id) {
        setFieldItem(FieldName.ID.getValue(), id);
    }

    public String getCheckDigit() {
        return getFieldItem(FieldName.CHECK_DIGIT.value);
    }

    public void setCheckDigit(String checkDigit) {
        setFieldItem(FieldName.CHECK_DIGIT.getValue(), checkDigit);
    }

    public String getCodeIdentifier() {
        return getFieldItem(FieldName.CODE_IDENTIFIER.value);
    }

    public void setCodeIdentifier(final String codeIdentifier) {
        setFieldItem(FieldName.CODE_IDENTIFIER.getValue(), codeIdentifier);
    }

    public String getAssigningAuthority() {
        return getFieldItem(FieldName.ASSIGNING_AUTHORITY.value);
    }

    public void setAssigningAuthority(final String assigningAuthority) {
        setFieldItem(FieldName.ASSIGNING_AUTHORITY.getValue(), assigningAuthority);
    }

    public String getIdentifierTypeCode() {
        return getFieldItem(FieldName.IDENTIFIER_TYPE_CODE.value);
    }

    public void setIdentifierTypeCode(final String identifierTypeCode) {
        setFieldItem(FieldName.IDENTIFIER_TYPE_CODE.getValue(), identifierTypeCode);
    }

    public String getAssigningFacility() {
        return getFieldItem(FieldName.ASSIGNING_FACILITY.value);
    }

    public void setAssigningFacility(final String assigningFacility) {
        setFieldItem(FieldName.ASSIGNING_FACILITY.getValue(), assigningFacility);
    }

    public String getEffectiveDate() {
        return getFieldItem(FieldName.EFFECTIVE_DATE.value);
    }

    public void setEffectiveDate(final String effectiveDate) {
        setFieldItem(FieldName.EFFECTIVE_DATE.getValue(), effectiveDate);
    }

    public String getExpirationDate() {
        return getFieldItem(FieldName.EXPIRATION_DATE.value);
    }

    public void setExpirationDate(final String expirationDate) {
        setFieldItem(FieldName.EXPIRATION_DATE.getValue(), expirationDate);
    }

    public String getAssigningJurisdiction() {
        return getFieldItem(FieldName.ASSIGNING_JURISDICTION.value);
    }

    public void setAssigningJurisdiction(String assigningJurisdiction) {
        setFieldItem(FieldName.ASSIGNING_JURISDICTION.getValue(), assigningJurisdiction);
    }

    public String getAssigningAgency() {
        return getFieldItem(FieldName.ASSIGNING_AGENCY.value);
    }

    public void setAssigningAgency(final String assigningAgency) {
        setFieldItem(FieldName.ASSIGNING_AGENCY.getValue(), assigningAgency);
    }
}
