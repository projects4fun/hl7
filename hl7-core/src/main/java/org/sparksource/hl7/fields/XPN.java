package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;

import java.util.logging.Logger;

/**
 * <p>The concrete field around Extended Person Name (XPN).</p>
 *
 * @author Juan Garcia
 * @since 2016-08-12
 */
public class XPN extends Field {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    private enum FieldName {
        FAMILY_NAME(0), GIVEN_NAME(1), MIDDLE_NAME(2),
        SUFFIX(3), PREFIX(4), DEGREE(5), NAME_TYPE_CODE(6),
        NAME_REPRESENTATION_CODE(7);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public XPN(final GenericMessage genericMessage) {
        super(genericMessage);
    }

    @Override
    public XPN build(final String field) {
        XPN xpn = new XPN(getMessage());
        if (field == null || field.trim().equals("")) {
            logger.finest("Returning empty XPN object for empty content.");
            return xpn;
        }

        final String[] fields = field.split(escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            xpn.setFieldItem(i, fields[i]);
        }

        return xpn;
    }

    public String getFamilyName() {
        return getFieldItem(FieldName.FAMILY_NAME.value);
    }

    public void setFamilyName(final String familyName) {
        setFieldItem(FieldName.FAMILY_NAME.getValue(), familyName);
    }

    public String getGivenName() {
        return getFieldItem(FieldName.GIVEN_NAME.value);
    }

    public void setGivenName(final String givenName) {
        setFieldItem(FieldName.GIVEN_NAME.getValue(), givenName);
    }

    public String getMiddleName() {
        return getFieldItem(FieldName.MIDDLE_NAME.value);
    }

    public void setMiddleName(final String middleName) {
        setFieldItem(FieldName.MIDDLE_NAME.getValue(), middleName);
    }

    public String getSuffix() {
        return getFieldItem(FieldName.SUFFIX.value);
    }

    public void setSuffix(final String suffix) {
        setFieldItem(FieldName.SUFFIX.getValue(), suffix);
    }

    public String getPrefix() {
        return getFieldItem(FieldName.PREFIX.value);
    }

    public void setPrefix(final String prefix) {
        setFieldItem(FieldName.PREFIX.getValue(), prefix);
    }

    public String getDegree() {
        return getFieldItem(FieldName.DEGREE.value);
    }

    public void setDegree(final String degree) {
        setFieldItem(FieldName.DEGREE.getValue(), degree);
    }

    public String getNameTypeCode() {
        return getFieldItem(FieldName.NAME_TYPE_CODE.value);
    }

    public void setNameTypeCode(final String nameTypeCode) {
        setFieldItem(FieldName.NAME_TYPE_CODE.getValue(), nameTypeCode);
    }

    public String getNameRepresentationCode() {
        return getFieldItem(FieldName.NAME_REPRESENTATION_CODE.value);
    }

    public void setNameRepresentationCode(final String nameRepresentationCode) {
        setFieldItem(FieldName.NAME_REPRESENTATION_CODE.getValue(), nameRepresentationCode);
    }
}
