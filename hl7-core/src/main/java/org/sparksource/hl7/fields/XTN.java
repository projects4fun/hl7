package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;

import java.util.logging.Logger;

/**
 * @author Juan Garcia
 * @since 2016-08-30
 */
public class XTN extends Field {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    private enum FieldName {
        TELEPHONE_NUMBER(0),
        TELECOMMUNICATION_USE_CODE(1),
        TELECOMMUNICATION_EQUIPMENT_TYPE(2),
        EMAIL_ADDRESS(3),
        COUNTRY_CODE(4),
        AREA_CODE(5),
        LOCAL_NUMBER(6),
        EXTENSION(7),
        ANY_TEXT(8),
        EXTENSION_PREFIX(9),
        SPEED_DIAL_CODE(10),
        UNFORMATTED_TELEPHONE_NUMBER(11);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public XTN(final GenericMessage message) {
        super(message);
    }

    @Override
    public XTN build(final String field) {
        XTN xtn = new XTN(getMessage());
        if (field == null || field.trim().equals("")) {
            logger.finest("Returning empty XTN object for empty content.");
            return xtn;
        }

        final String[] fields = field.split(escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            xtn.setFieldItem(i, fields[i]);
        }

        return xtn;
    }

    public String getTelephoneNumber() {
        return getFieldItem(FieldName.TELEPHONE_NUMBER.value);
    }

    public void setTelephoneNumber(final String telephoneNumber) {
        setFieldItem(FieldName.TELEPHONE_NUMBER.getValue(), telephoneNumber);
    }

    public String getTelecommunicationUseCode() {
        return getFieldItem(FieldName.TELECOMMUNICATION_USE_CODE.value);
    }

    public void setTelecommunicationUseCode(final String telecommunicationUseCode) {
        setFieldItem(FieldName.TELECOMMUNICATION_USE_CODE.getValue(), telecommunicationUseCode);
    }

    public String getTelecommunicationEquipmentType() {
        return getFieldItem(FieldName.TELECOMMUNICATION_EQUIPMENT_TYPE.value);
    }

    public void setTelecommunicationEquipmentType(final String telecommunicationEquipmentType) {
        setFieldItem(FieldName.TELECOMMUNICATION_EQUIPMENT_TYPE.value, telecommunicationEquipmentType);
    }

    public String getEmailAddress() {
        return getFieldItem(FieldName.EMAIL_ADDRESS.value);
    }

    public void setEmailAddress(final String emailAddress) {
        setFieldItem(FieldName.EMAIL_ADDRESS.getValue(), emailAddress);
    }

    public String getCountryCode() {
        return getFieldItem(FieldName.COUNTRY_CODE.value);
    }

    public void setCountryCode(final String countryCode) {
        setFieldItem(FieldName.COUNTRY_CODE.getValue(), countryCode);
    }

    public String getAreaCode() {
        return getFieldItem(FieldName.AREA_CODE.value);
    }

    public void setAreaCode(final String areaCode) {
        setFieldItem(FieldName.AREA_CODE.getValue(), areaCode);
    }

    public String getLocalNumber() {
        return getFieldItem(FieldName.LOCAL_NUMBER.value);
    }

    public void setLocalNumber(final String localNumber) {
        setFieldItem(FieldName.LOCAL_NUMBER.getValue(), localNumber);
    }

    public String getExtension() {
        return getFieldItem(FieldName.EXTENSION.value);
    }

    public void setExtension(final String extension) {
        setFieldItem(FieldName.EXTENSION.getValue(), extension);
    }

    public String getAnyText() {
        return getFieldItem(FieldName.ANY_TEXT.value);
    }

    public void setAnyText(final String anyText) {
        setFieldItem(FieldName.ANY_TEXT.getValue(), anyText);
    }

    public String getExtensionPrefix() {
        return getFieldItem(FieldName.EXTENSION_PREFIX.value);
    }

    public void setExtensionPrefix(final String extensionPrefix) {
        setFieldItem(FieldName.EXTENSION_PREFIX.getValue(), extensionPrefix);
    }

    public String getSpeedDialCode() {
        return getFieldItem(FieldName.SPEED_DIAL_CODE.value);
    }

    public void setSpeedDialCode(final String speedDialCode) {
        setFieldItem(FieldName.SPEED_DIAL_CODE.getValue(), speedDialCode);
    }

    public String getUnformattedTelephoneNumber() {
        return getFieldItem(FieldName.UNFORMATTED_TELEPHONE_NUMBER.value);
    }

    public void setUnformattedTelephoneNumber(final String unformattedTelephoneNumber) {
        setFieldItem(FieldName.UNFORMATTED_TELEPHONE_NUMBER.getValue(), unformattedTelephoneNumber);
    }
}
