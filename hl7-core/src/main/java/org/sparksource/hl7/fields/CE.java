package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.segments.GenericSegment;

import java.util.logging.Logger;

/**
 * @author Juan Garcia
 * @since 2016-08-23
 */
public class CE extends Field {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    public enum FieldName {
        IDENTIFIER(0), TEXT(1), NAME_OF_CODING_SYSTEM(2),
        ALTERNATE_IDENTIFIER(3), ALTERNATE_TEXT(4),
        NAME_OF_ALTERNATE_CODING_SYSTEM(5);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public CE(final GenericMessage message) {
        super(message);
    }

    @Override
    public CE build(final String field) {
        CE ce = new CE(getMessage());

        if (field == null || field.trim().equals("")) {
            logger.finer("CE field was empty string or null");
            return ce;
        }

        String[] fields = field.split(GenericSegment.escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            ce.setFieldItem(i, fields[i]);
        }

        return ce;
    }

    public String getIdentifier() {
        return getFieldItem(FieldName.IDENTIFIER.value);
    }

    public void setIdentifier(final String identifier) {
        setFieldItem(FieldName.IDENTIFIER.getValue(), identifier);
    }

    public String getText() {
        return getFieldItem(FieldName.TEXT.value);
    }

    public void setText(final String text) {
        setFieldItem(FieldName.TEXT.getValue(), text);
    }

    public String getNameOfCodingSystem() {
        return getFieldItem(FieldName.NAME_OF_CODING_SYSTEM.value);
    }

    public void setNameOfCodingSystem(final String nameOfCodingSystem) {
        setFieldItem(FieldName.NAME_OF_CODING_SYSTEM.getValue(), nameOfCodingSystem);
    }

    public String getAlternateIdentifier() {
        return getFieldItem(FieldName.ALTERNATE_IDENTIFIER.value);
    }

    public void setAlternateIdentifier(final String alternateIdentifier) {
        setFieldItem(FieldName.ALTERNATE_IDENTIFIER.getValue(), alternateIdentifier);
    }

    public String getAlternateText() {
        return getFieldItem(FieldName.ALTERNATE_TEXT.value);
    }

    public void setAlternateText(final String alternateText) {
        setFieldItem(FieldName.ALTERNATE_TEXT.getValue(), alternateText);
    }

    public String getNameOfAlternateCodingSystem() {
        return getFieldItem(FieldName.NAME_OF_ALTERNATE_CODING_SYSTEM.value);
    }

    public void setNameOfAlternateCodingSystem(final String nameOfAlternateCodingSystem) {
        setFieldItem(FieldName.NAME_OF_ALTERNATE_CODING_SYSTEM.getValue(), nameOfAlternateCodingSystem);
    }
}
