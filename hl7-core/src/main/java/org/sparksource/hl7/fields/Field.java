package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.segments.GenericSegment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

/**
 * @author Juan Garcia
 * @since 2016-04-14
 */
public abstract class Field extends GenericSegment {

    final public static String DEFAULT_COMPONENT_DELIMITER = "^";

    public Field(final GenericMessage message) {
        super(message);
    }

    public abstract Field build(final String line);

    private List<String> fieldList;

    void setFieldItem(final int position, String value) {
        if (getFieldList().size() < (position + 1)) {
            expandFieldsItem(position);
        }

        value = unescapeCharacterSequence(value);

        getFieldList().set(position, value);
    }


    /**
     * <p>Obtains a value from the {@link #fieldList}. This method ensures that
     * a null value is not provided as a response a valid response is a empty
     * {@link String} or the {@link String} on the {@link List}.</p>
     *
     * @param position the index value from the list to retrieve
     * @return content on the list index; empty string if value is not present
     */
    String getFieldItem(final int position) {
        if (getFieldList().size() >= position + 1) {
            return getFieldList().get(position);
        } else {
            return "";
        }
    }

    private void expandFieldsItem(final int position) {
        while (getFieldList().size() <= position) {
            getFieldList().add("");
        }
    }

    /**
     * <p>All characters used inside the HL7 control characters must be escaped
     * when used as a part of text in a field.</p>
     *
     * @param value the {@link String} that may require an escape operation
     *              performed on
     * @return the {@link String} with sequences undone
     */
    protected String unescapeCharacterSequence(String value) {
        // return an empty string for null or empty strings
        if (value == null || value.isEmpty()) return "";

        // if an escape character is not found return the value as is
        if (!value.contains(getMessage().getEscapeCharacter())) {
            return value;
        }

        for (Map.Entry<String, String> escapeSequence : getMessage().getEscapeSequences().entrySet()) {
            if (value.contains(escapeSequence.getKey())) {
                value = value.replace(escapeSequence.getKey(), escapeSequence.getValue());
            }
        }

        return value;
    }

    /**
     * <p>Provides the capability to escape character sequences. This task is
     * is / should be performed when a HL7 object is being transformed to a
     * {@link String}.</p>
     *
     * <p>This method can not take advantage of the single character elimination
     * that {@link #unescapeCharacterSequence(String)} uses due to having to
     * check for multiple characters.</p>
     *
     * <p>This method is only called from {@link Field#toString()}. Calling this
     * method from {@link GenericSegment#toString()} would cause all delimiters
     * such as ^, &amp;, \, and ~ to be escaped incorrectly. Segments should be
     * updated to use types for all {@link Field} operations for better support
     * of escape sequences.</p>
     *
     * @param value the {@link String} that may require escaping
     * @return the {@link String} with escapes performed
     */
    protected String escapeCharacterSequence(String value) {
        // return an empty string for null or empty strings
        if (value == null || value.isEmpty()) return "";

        for (Map.Entry<String, String> escapeSequence : getMessage().getEscapeSequences().entrySet()) {
            if (value.contains(escapeSequence.getValue())) {
                value = value.replace(escapeSequence.getValue(), escapeSequence.getKey());
            }
        }

        return value;
    }

    /**
     * <p>Returns the backing {@link #fieldList} for a field. Should it be null
     * it assigns a new {@link ArrayList} prior to returning the variable.</p>
     *
     * @return the backing {@link #fieldList} for the current data type
     */
    public List<String> getFieldList() {
        if (fieldList == null) {
            fieldList = new ArrayList<>();
        }

        return fieldList;
    }

    @Override
    public String toString() {
        final String delimiter;
        if (getMessage() == null) {
            delimiter = DEFAULT_COMPONENT_DELIMITER;
        } else {
            delimiter = getMessage().getComponentDelimiter();
        }

        StringJoiner stringJoiner = new StringJoiner(delimiter);
        for (final String s : getFieldList()) {
            stringJoiner.add(s == null ? "" : escapeCharacterSequence(s));
        }

        return stringJoiner.toString();
    }
}
