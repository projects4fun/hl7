package org.sparksource.hl7.fields;

import org.sparksource.hl7.GenericMessage;

import java.util.logging.Logger;

/**
 * <p>A concrete field around Composite (CM).</p>
 *
 * @author Juan Garcia
 * @since 2017-08-15
 */
public class CM extends Field {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    public enum FieldName {
        EVENT_TYPE(0), EVENT_CODE(1);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public CM(final GenericMessage genericMessage) {
        super(genericMessage);
    }

    @Override
    public CM build(final String component) {
        CM cm = new CM(getMessage());
        if (component == null || component.equals("")) {
            logger.finest("Returning empty CM object for empty content.");
            return cm;
        }

        final String[] components = component.split(escapeDelimiter(getMessage()
            .getComponentDelimiter()));

        for (int i = 0; i < components.length; i++) {
            cm.setFieldItem(i, components[i]);
        }

        return cm;
    }

    public String getEventType() {
        return getFieldItem(FieldName.EVENT_TYPE.value);
    }

    public void setEventType(final String eventType) {
        setFieldItem(FieldName.EVENT_TYPE.getValue(), eventType);
    }

    public String getEventCode() {
        return getFieldItem(FieldName.EVENT_CODE.value);
    }

    public void setEventCode(final String eventCode) {
        setFieldItem(FieldName.EVENT_CODE.getValue(), eventCode);
    }
}
