package org.sparksource.hl7;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

/**
 * <p>Performs writing actions on a {@link GenericMessage}.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-30
 */
public interface MessageWriter {

    /**
     * <p>Provides the means to take a {@link GenericMessage} and write it to a defined
     * {@link Path}.</p>
     *
     * @param path the path to write a {@link GenericMessage} to
     * @throws IOException if the file is a directory;
     *                     if the file cannot be created;
     *                     if the cannot be opened;
     */
    default void toFile(final Path path) throws IOException {
        if (path == null) {
            throw new IOException("path must not be null.");
        }

        final String message = this.toString();
        try (FileWriter fileWriter = new FileWriter(path.toFile())) {
            fileWriter.write(message);
        }
    }

    /**
     * <p>Provides the means to take a {@link GenericMessage} and write it to a defined
     * {@link File}.</p>
     *
     * @param file the path to write a {@link GenericMessage} to
     * @throws IOException if the file is a directory;
     *                     if the file cannot be created;
     *                     if the cannot be opened;
     */
    default void toFile(final File file) throws IOException {
        if (file == null) {
            throw new IOException("file must not be null.");
        }

        toFile(file.toPath());
    }

    /**
     * <p>Provides the means to take a {@link GenericMessage} and write it to a defined
     * {@link String} location.</p>
     *
     * @param location the path to write a {@link GenericMessage} to
     * @throws IOException if the file is a directory;
     *                     if the file cannot be created;
     *                     if the cannot be opened;
     */
    default void toFile(final String location) throws IOException {
        if (location == null || location.trim().equals("")) {
            throw new IOException("location must not be null or empty string.");
        }

        toFile(new File(location).toPath());
    }
}
