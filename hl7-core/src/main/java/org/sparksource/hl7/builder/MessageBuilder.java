package org.sparksource.hl7.builder;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.generators.IdGenerator;
import org.sparksource.hl7.generators.MessageControlIdGenerator;
import org.sparksource.hl7.segments.MSH;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

/**
 * <p>Provides quick start static methods for creating HL7 messages.</p>
 *
 * @author Juan Garcia
 * @since 2019-04-22
 */
public class MessageBuilder {

    final private static Logger logger =
        Logger.getLogger(MessageBuilder.class.getName());

    final protected static DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    /**
     * <p>Prevent instantiation.</p>
     */
    protected MessageBuilder() {
    }

    /**
     * <p>Default {@link GenericMessage} creator with a {@link MSH} entry. The
     * caller has to implement the remaining parts.</p>
     *
     * @return a new minimal {@link GenericMessage}
     */
    public static GenericMessage newInstance() {
        logger.finest("Creating default generic message");

        GenericMessage genericMessage = new GenericMessage();

        MSH msh = new MSH(genericMessage);
        msh.setEncodingCharacters("^~\\&");
        msh.setMessageControlId(MessageControlIdGenerator.next());
        msh.setSequenceNumber(IdGenerator.next());
        msh.setDateTime(formatter.format(OffsetDateTime.now()));

        genericMessage.getSegments().add(msh);

        return genericMessage;
    }
}
