package org.sparksource.hl7;

import org.sparksource.hl7.parser.Slicer;
import org.sparksource.hl7.segments.MSH;
import org.sparksource.hl7.segments.Segment;

import java.util.List;

/**
 * <p>Base set of information that all implementations of a {@link Message}
 * should expose.</p>
 *
 * @author Juan Garcia
 * @since 2019-04-22
 */
public interface Message<T> extends MessageWriter {

    T build(List<String> lines) throws IllegalMessageFormat;

    /**
     * <p>All HL7 messages must have exactly one {@link MSH} as the first item
     * and contains information that is helpful throughout the processing of a
     * {@link Message}. All implementations should implement this.</p>
     *
     * @return the {@link MSH} of the current {@link Message}
     */
    MSH getMSH();

    /**
     * <p>The HL7 version that a {@link Message} class is implementing i.e 2.5.1
     * when a artifact is detected in the classpath that supports a particular
     * version that class will be used instead of {@link GenericMessage} when
     * a HL7 message is created.</p>
     *
     * @return the HL7 version a class is implementing
     */
    String getVersionImpl();

    /**
     * <p>Provides the field delimiter that should be used for this HL7 message.
     * HL7 recommends that this value be the pipe {|} character.</p>
     *
     * <p>Fun fact. The first pipe of a HL7 message is MSH-1. MSH-2 are the
     * encoding characters. A simple string.split("|") is not enough.</p>
     *
     * @return the field delimiter for this message
     */
    String getFieldDelimiter();

    /**
     * <p>Defines the field delimiter to be used for the current HL7 message
     * being parsed.</p>
     *
     * @param fieldDelimiter the field delimiter for this message
     */
    void setFieldDelimiter(final String fieldDelimiter);

    /**
     * <p>Provides the component delimiter that should be used for this HL7
     * message. Hl7 recommends that this value be the hat (^) character.</p>
     *
     * @return the component delimiter for this message
     */
    String getComponentDelimiter();

    /**
     * <p>Defines the field delimiter to be used for the current HL7 message
     * being parsed.</p>
     *
     * @param componentDelimiter the component delimiter
     */
    void setComponentDelimiter(final String componentDelimiter);

    /**
     * <p>Provides the sub component delimiter that should be used for this HL7
     * message. HL7 recommends that this value be the ampersand (&amp;) character.</p>
     *
     * @return the sub component delimiter for this message
     */
    String getSubComponentDelimiter();

    /**
     * <p>Defines the sub component delimiter to be used for the current HL7
     * message being parsed.</p>
     *
     * @param subComponentDelimiter the sub component delimiter
     */
    void setSubComponentDelimiter(final String subComponentDelimiter);

    /**
     * <p>Provides the field repeating delimiter that should be used for this
     * HL7 message. HL7 recommends that this value be the tilde (~) character.</p>
     *
     * @return the field repeat delimiter
     */
    String getRepeatDelimiter();

    /**
     * <p>Defines the field repeat delimiter to be used for the current HL7
     * message being parsed.</p>
     *
     * @param repeatDelimiter the field repeat delimiter
     */
    void setRepeatDelimiter(final String repeatDelimiter);

    /**
     * <p>Provides the escape character that should be used for this HL7
     * message. HL7 recommends that this value be the backslash (\) character.</p>
     *
     * @return the escape character
     */
    String getEscapeCharacter();

    /**
     * <p>Defines the escape character to be used for the current HL7 message
     * being parsed.</p>
     *
     * @param escapeCharacter the escape character
     */
    void setEscapeCharacter(final String escapeCharacter);

    /**
     * <p>All HL7 messages will contain a back {@link List} of the segments at
     * hand. Clients using a {@link Message} need to have a means of altering
     * contents. An implementor should provide the backing {@link List} of all
     * segments.</p>
     *
     * @return the {@link Segment} list that create belong to {@link Message}
     */
    List<Segment> getSegments();

    /**
     * <p>Appends to the end of a {@link List} a {@link Segment}. A caller using
     * this method must ensure that the {@link Segment} being added is in the
     * correct location - this method makes no guarantees that a addition is in
     * the proper location according to specifications.</p>
     *
     * @param segment the {@link Segment} to be added to a {@link Message}
     * @return the current {@link Message} implementation
     */
    T addSegment(final Segment segment);

    /**
     * <p>Obtains the {@link Slicer} with this {@link Message} in context.</p>
     *
     * @return a initialized {@link Slicer} instance
     */
    Slicer getSlicer();
}
