package org.sparksource.hl7;

import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.parser.Slicer;
import org.sparksource.hl7.segments.MSH;
import org.sparksource.hl7.segments.Segment;

import java.util.List;

/**
 * <p>Handles the base construction / consumption of a HL7 {@link GenericMessage}.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-11
 */
public class GenericMessage extends AbstractMessage<GenericMessage> {

    private String fieldDelimiter = "|";
    private String componentDelimiter = "^";
    private String subComponentDelimiter = "&";
    private String repeatDelimiter = "~";
    private String escapeCharacter = "\\";

    @Override
    public GenericMessage build(final List<String> lines) throws IllegalMessageFormat {
        return Parse.from(lines);
    }

    /**
     * <p>All HL7 messages must have exactly one {@link MSH} as the first item
     * and contains information that is helpful throughout the processing of a
     * {@link Message}. All implementations should implement this.</p>
     *
     * @return the {@link MSH} of the current {@link Message}
     */
    @Override
    public MSH getMSH() {
        return (MSH) getSegments().get(0);
    }

    /**
     * <p>The HL7 version that a {@link Message} class is implementing i.e 2.5.1
     * when a artifact is detected in the classpath that supports a particular
     * version that class will be used instead of {@link GenericMessage} when
     * a HL7 message is created.</p>
     *
     * <p>This {@link GenericMessage} returns a value of 2.X.X as it is meant to
     * handle HL7 message in the most basic of forms.</p>
     *
     * @return the HL7 version a class is implementing
     */
    @Override
    public String getVersionImpl() {
        return "2.X.X";
    }

    /**
     * <p>Provides the field delimiter that should be used for this HL7 message.
     * HL7 recommends that this value be the pipe {|} character.</p>
     *
     * <p>Fun fact. The first pipe of a HL7 message is MSH-1. MSH-2 are the
     * encoding characters. A simple string.split("|") is not enough.</p>
     *
     * @return the field delimiter for this message
     */
    public String getFieldDelimiter() {
        return fieldDelimiter;
    }

    /**
     * <p>Defines the field delimiter to be used for the current HL7 message
     * being parsed.</p>
     *
     * @param fieldDelimiter the field delimiter for this message
     */
    public void setFieldDelimiter(String fieldDelimiter) {
        this.fieldDelimiter = fieldDelimiter;
    }

    /**
     * <p>Provides the component delimiter that should be used for this HL7
     * message. Hl7 recommends that this value be the hat (^) character.</p>
     *
     * @return the component delimiter for this message
     */
    public String getComponentDelimiter() {
        return componentDelimiter;
    }

    /**
     * <p>Defines the field delimiter to be used for the current HL7 message
     * being parsed.</p>
     *
     * @param componentDelimiter the component delimiter
     */
    public void setComponentDelimiter(String componentDelimiter) {
        this.componentDelimiter = componentDelimiter;
    }

    /**
     * <p>Provides the sub component delimiter that should be used for this HL7
     * message. HL7 recommends that this value be the ampersand (&amp;) character.</p>
     *
     * @return the sub component delimiter for this message
     */
    public String getSubComponentDelimiter() {
        return subComponentDelimiter;
    }

    /**
     * <p>Defines the sub component delimiter to be used for the current HL7
     * message being parsed.</p>
     *
     * @param subComponentDelimiter the sub component delimiter
     */
    public void setSubComponentDelimiter(String subComponentDelimiter) {
        this.subComponentDelimiter = subComponentDelimiter;
    }

    /**
     * <p>Provides the field repeating delimiter that should be used for this
     * HL7 message. HL7 recommends that this value be the tilde (~) character.</p>
     *
     * @return the field repeat delimiter
     */
    public String getRepeatDelimiter() {
        return repeatDelimiter;
    }

    /**
     * <p>Defines the field repeat delimiter to be used for the current HL7
     * message being parsed.</p>
     *
     * @param repeatDelimiter the field repeat delimiter
     */
    public void setRepeatDelimiter(String repeatDelimiter) {
        this.repeatDelimiter = repeatDelimiter;
    }

    /**
     * <p>Provides the escape character that should be used for this HL7
     * message. HL7 recommends that this value be the backslash (\) character.</p>
     *
     * @return the escape character
     */
    public String getEscapeCharacter() {
        return escapeCharacter;
    }

    /**
     * <p>Defines the escape character to be used for the current HL7 message
     * being parsed.</p>
     *
     * @param escapeCharacter the escape character
     */
    public void setEscapeCharacter(String escapeCharacter) {
        this.escapeCharacter = escapeCharacter;
    }

    /**
     * <p>Appends to the end of a {@link List} a {@link Segment}. A caller using
     * this method must ensure that the {@link Segment} being added is in the
     * correct location - this method makes no guarantees that a addition is in
     * the proper location according to specifications.</p>
     *
     * @param segment the {@link Segment} to be added to a {@link Message}
     * @return the current {@link Message} implementation
     */
    public GenericMessage addSegment(final Segment segment) {
        this.getSegments().add(segment);
        return this;
    }

    /**
     * <p>Obtains the {@link Slicer} with this {@link Message} in context.</p>
     *
     * @return a initialized {@link Slicer} instance
     */
    public Slicer getSlicer() {
        return new Slicer(this);
    }
}
