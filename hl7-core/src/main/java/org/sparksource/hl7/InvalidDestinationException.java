package org.sparksource.hl7;

/**
 * <p>Exception around the attempt to send a HL7 message.</p>
 *
 * @author Juan Garcia
 * @since 2017-07-24
 */
public class InvalidDestinationException extends Exception {

    public InvalidDestinationException(final String s) {
        super(s);
    }
}
