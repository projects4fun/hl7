package org.sparksource.hl7;

import org.sparksource.hl7.parser.Parse;
import org.sparksource.hl7.security.TlsConfiguration;
import org.sparksource.hl7.security.TlsConfigurationException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * @author Juan Garcia
 * @since 2016-04-19
 */
public class MllpSender implements AutoCloseable {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    private Socket socket;
    private OutputStream socketOutputStream;
    private InputStream socketInputStream;

    final private String hostname;
    final private int port;
    private TlsConfiguration tlsConfiguration;
    private int connectionTimeout = 30;
    private int soTimeout = 20;
    private int retryAttempts = 3;
    private int retryIntervals = 10;

    /**
     * <p>Creates a HL7 sender that does not use SSL / TLS for transmission with
     * the remote server. This should only be used when the remote server does
     * not support SSL / TLS.</p>
     *
     * @param hostname the hostname of the remote server
     * @param port     the port that the remote server is listening on
     */
    public MllpSender(final String hostname, final int port) {
        this.hostname = hostname;
        this.port = port;
    }

    /**
     * <p>Creates a HL7 sender that does not use SSL / TLS for transmission with
     * the remote server. This should only be used when the remote server does
     * not support SSL / TLS. This is a convenience method to allow configuration
     * such as "servername:8000".</p>
     *
     * @param destination the remote server name and port combination
     * @throws InvalidDestinationException if the extracted input destination is
     *                                     not at most two items
     */
    public MllpSender(final String destination) throws InvalidDestinationException {
        final String[] destinationParts = destination.split(":");

        if (destinationParts.length != 2) {
            throw new InvalidDestinationException("Provided destination of " +
                destination + " is not valid.");
        }

        this.hostname = destinationParts[0];
        this.port = Integer.parseInt(destinationParts[1]);
    }

    /**
     * <p>Creates a HL7 sender that uses SSL / TLS for transmission with the
     * remote server.</p>
     *
     * @param hostname      the hostname of the remote server
     * @param port          the port that the remote server is listening on
     * @param configuration the {@link TlsConfiguration} to authenticate the
     *                      remote hostname
     */
    public MllpSender(final String hostname, final int port,
                      final TlsConfiguration configuration) {
        this.hostname = hostname;
        this.port = port;
        this.tlsConfiguration = configuration;
    }

    /**
     * <p>Creates a basic HL7 sender that uses SSL / TLS for transmission with
     * the remote server. This is a convenience method to allow configuration
     * such as "servername:8000".</p>
     *
     * @param destination   the remote server name and port combination
     * @param configuration the {@link TlsConfiguration} to authenticate the
     *                      remote hostname
     * @throws InvalidDestinationException if the extracted input destination is
     *                                     not at most two items
     */
    public MllpSender(final String destination, final TlsConfiguration configuration)
        throws InvalidDestinationException {
        final String[] destinationParts = destination.split(":");

        if (destinationParts.length != 2) {
            throw new InvalidDestinationException("Provided destination of " +
                destination + " is not valid.");
        }

        this.hostname = destinationParts[0];
        this.port = Integer.parseInt(destinationParts[1]);

        this.tlsConfiguration = configuration;
    }

    /**
     * <p>Takes a {@link String} that is an HL7 message and sends it to the
     * remote site. This uses {@link MllpStream} as the final converter from the
     * value that represents our data to the value that is a valid HL7
     * message.</p>
     *
     * @param message the {@link String} that is to be sent outbound
     * @return the response from the remote site as a {@link GenericMessage}
     * @throws IOException               if the connection is refused by the remote server
     * @throws IllegalMessageFormat      if the response fails to parse into a {@link GenericMessage} object
     * @throws TlsConfigurationException if a configuration was provided and it is invalid
     * @throws InterruptedException      is this thread is killed while waiting to send a failed message
     */
    public GenericMessage send(final String message)
        throws IOException, IllegalMessageFormat, TlsConfigurationException, InterruptedException {

        validateSocketConnection();

        for (int i = 0; i < retryAttempts; i++) {
            try {
                MllpStream.write(socketOutputStream, message);
                break;
            } catch (IOException e) {
                logger.warning("Remote site refused our connection. Closing existing socket and trying again.");
                // allows the socket recreation by validateSocketConnection()
                socket.close();
                // sleep before trying to renew a connection
                // i.e do not spam when the connection fails before trying again
                Thread.sleep(TimeUnit.SECONDS.toMillis(retryIntervals * i));
                validateSocketConnection();
            }
        }

        GenericMessage response = Parse.from(MllpStream.read(socketInputStream));
        logger.finer(() -> String.format("built response object: \n%s", response));

        return response;
    }

    /**
     * <p>A convenience method to the internal {@link #send(String)}. The internal method takes a
     * {@link String} that {@link GenericMessage} supports over its {@link GenericMessage#toString()}
     * implementation.</p>
     *
     * @param message the object of type {@link GenericMessage} that is to be sent outbound
     * @return the response from the remote site as a {@link GenericMessage}
     * @throws IOException               if the connection is refused by the remote server
     * @throws IllegalMessageFormat      if the response fails to parse into a {@link GenericMessage} object
     * @throws TlsConfigurationException if a configuration was provided and it is invalid
     * @throws InterruptedException      is this thread is killed while waiting to send a message
     */
    public GenericMessage send(final GenericMessage message)
        throws IOException, IllegalMessageFormat, TlsConfigurationException, InterruptedException {

        return send(message.toString());
    }

    /**
     * <p>Creates a {@link Socket} that can be used to send data. If a {@link TlsConfiguration} is
     * provided to the {@link MllpSender} constructor a {@link SSLSocket} is generated instead to
     * secure the connection.</p>
     *
     * @return an object of either {@link Socket} or {@link SSLSocket}
     * @throws IOException               if {@link Socket} fails to connect to the remote site
     * @throws TlsConfigurationException if the provided {@link TlsConfiguration} is invalid.
     */
    private Socket createSocket() throws IOException, TlsConfigurationException {
        if (tlsConfiguration != null) {
            SSLContext sslContext = tlsConfiguration.buildSSLContext();
            SSLSocket sslSocket = (SSLSocket) sslContext.getSocketFactory().createSocket();

            // timeout configurations
            sslSocket.connect(new InetSocketAddress(hostname, port),
                (int) TimeUnit.SECONDS.toMillis(connectionTimeout));
            sslSocket.setSoTimeout((int) TimeUnit.SECONDS.toMillis(soTimeout));

            return sslSocket;
        }

        return new Socket(hostname, port);
    }

    /**
     * <p>Performs initial creation of a {@link Socket} and future recreation of a {@link Socket}
     * when a failure to write to a remote site fails.</p>
     *
     * @throws IOException if the connection and its {@link InputStream} or {@link OutputStream}
     *                     fail to be obtained
     */
    private void validateSocketConnection() throws IOException, TlsConfigurationException {
        if (socket == null || socket.isClosed()) {
            logger.finer("socket was null or closed. creating a new one.");
            socket = createSocket();
            socket.setReuseAddress(true);

            socketOutputStream = socket.getOutputStream();
            socketInputStream = socket.getInputStream();
        }
    }

    // =========================================
    // =========================================

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public void setSoTimeout(int soTimeout) {
        this.soTimeout = soTimeout;
    }

    public int getRetryAttempts() {
        return retryAttempts;
    }

    public void setRetryAttempts(int retryAttempts) {
        this.retryAttempts = retryAttempts;
    }

    public int getRetryIntervals() {
        return retryIntervals;
    }

    public void setRetryIntervals(int retryIntervals) {
        this.retryIntervals = retryIntervals;
    }

    // =========================================
    // =========================================

    @Override
    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
    }
}
