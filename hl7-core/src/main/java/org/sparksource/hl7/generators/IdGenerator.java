package org.sparksource.hl7.generators;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>A store for values that could be used in the HL7 message control id. This
 * store writes the current sequence to a ".id_store" to try and maintain a last
 * value across application restarts. If this file does not exist a new one is
 * generated.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-14
 */
public class IdGenerator {

    final private static Logger logger = Logger.getLogger(IdGenerator.class.getName());
    final static String ID_STORE = ".id_store";

    private static long id;

    /**
     * <p>Provides access to the next numerical value that an HL7 message could use in the message
     * control id.</p>
     *
     * @return the next value to be used from this {@link IdGenerator}
     */
    public synchronized static long next() {
        if (id == 0) {
            id = loadIdStore();
        }

        id += 1;

        try (FileOutputStream stream = new FileOutputStream(ID_STORE)) {
            stream.write(Long.toString(id).getBytes());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "", e);
        }

        return id;
    }

    /**
     * <p>Internal method to load the last known value after an application
     * restart.</p>
     *
     * @return the value stored in the file; default 1 if the file was not found
     */
    private static long loadIdStore() {
        final File file = new File(ID_STORE);

        if (!file.exists()) {
            return 1;
        }

        try {
            String s = new String(Files.readAllBytes(file.toPath()));
            return Long.parseLong(s);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "", e);
            return 1;
        }
    }
}
