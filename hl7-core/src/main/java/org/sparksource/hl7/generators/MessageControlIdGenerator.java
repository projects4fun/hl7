package org.sparksource.hl7.generators;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>A generation store for numeric values that could be used in a HL7 message
 * control id. This store generates values using the current date and appends a
 * value to it. A message control id is not suppose </p>
 *
 * @author Juan Garcia
 * @since 2017-11-14
 */
public class MessageControlIdGenerator {

    final private static DateTimeFormatter MINUTE_FORMATTER =
        DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    private static long id;

    /**
     * <p>Provides access to the next numerical value that an HL7 message could
     * use in the message control id.</p>
     *
     * @return the next value to be used from this {@link MessageControlIdGenerator}
     */
    public synchronized static String next() {
        id += 1;
        if (id >= 999999) {
            id = 0;
        }

        return MINUTE_FORMATTER.format(OffsetDateTime.now()) + id;
    }
}
