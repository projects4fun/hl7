package org.sparksource.hl7.security;

/**
 * <p>Exception around the attempt to use a {@link TlsConfiguration}.</p>
 *
 * @author Juan Garcia
 * @since 2017-07-24
 */
public class TlsConfigurationException extends Exception {

    public TlsConfigurationException(final Exception e) {
        super(e);
    }

    public TlsConfigurationException(final String s) {
        super(s);
    }

    public TlsConfigurationException(final String s, final Exception e) {
        super(s, e);
    }
}
