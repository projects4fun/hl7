package org.sparksource.hl7.security;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;

/**
 * <p>Expose the API to be used when needing to perform communication over SSL / TLS.</p>
 *
 * @author Juan Garcia
 * @since 2017-07-24
 */
public interface TlsConfiguration {

    SSLServerSocketFactory buildSSLServerSocketFactory() throws TlsConfigurationException;

    SSLSocketFactory buildSSLSocketFactory() throws TlsConfigurationException;

    SSLContext buildSSLContext() throws TlsConfigurationException;
}
