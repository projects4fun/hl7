package org.sparksource.hl7.parser;

/**
 * <p>Used to indicate that the query provided is not valid.</p>
 *
 * @author Juan Garcia
 * @since 2017-09-10
 */
public class InvalidFieldQueryException extends Exception {

    public InvalidFieldQueryException(final String s) {
        super(s);
    }
}
