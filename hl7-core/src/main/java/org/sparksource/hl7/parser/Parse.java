package org.sparksource.hl7.parser;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;
import org.sparksource.hl7.Message;
import org.sparksource.hl7.segments.GenericSegment;
import org.sparksource.hl7.segments.MSH;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Parse {

    final private static Logger logger = Logger.getLogger(GenericMessage.class.getName());
    final private static Pattern NEW_LINE_PAT = Pattern.compile("\r\n|\r|\n");
    final private static Properties properties = new Properties();

    static {
        try (InputStream stream = GenericMessage.class.getResourceAsStream(
                "/org/sparksource/hl7/parser/segments.properties")) {
            properties.load(stream);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "", e);
        }
    }

    /**
     * <p>Prevent instantiation.</p>
     */
    private Parse() {
    }

    public static GenericMessage from(final List<String> lines) throws IllegalMessageFormat {
        return build(lines);
    }

    /**
     * <p>Creates a {@link GenericMessage} from a {@link String}. The method supports
     * non standard line delimiters via {@link #NEW_LINE_PAT}.</p>
     *
     * @param string a {@link String} to be transformed to a {@link GenericMessage}.
     * @return a {@link GenericMessage} after parsing
     * @throws IllegalMessageFormat if a {@link GenericSegment} is not valid
     */
    public static GenericMessage from(final String string) throws IllegalMessageFormat {
        logger.finest(() -> String.format("Loading HL7 message from string object: %s",
            string));
        List<String> lines = Arrays.asList(NEW_LINE_PAT.split(string));
        return build(lines);
    }

    /**
     * <p>Creates a {@link GenericMessage} from a {@link InputStream}. The
     * method loads the entire stream and is passed to {@link #from(String)}.</p>
     *
     * @param stream {@link InputStream} to be transformed into a {@link GenericMessage}
     * @return a {@link GenericMessage} after parsing
     * @throws IllegalMessageFormat if a {@link GenericSegment} is not valid
     * @throws IOException          if the {@link InputStream} cannot be read
     */
    public static GenericMessage from(final InputStream stream)
        throws IllegalMessageFormat, IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = stream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, len);
            }

            return from(outputStream.toString(StandardCharsets.UTF_8.name()));
        }
    }

    /**
     * <p>Creates a {@link GenericMessage} from a {@link File}. The method reads the
     * entire file and passes it to {@link #from(InputStream)}.</p>
     *
     * @param file {@link File} containing the {@link GenericMessage} as a {@link String}
     * @return a {@link GenericMessage} after parsing
     * @throws IllegalMessageFormat if a {@link GenericSegment} is not valid
     * @throws IOException          if the {@link File} cannot be found
     */
    public static GenericMessage from(final File file)
        throws IllegalMessageFormat, IOException {
        logger.finest(() -> String.format("Loading HL7 message from file object: %s",
            file.getAbsolutePath()));

        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            return from(fileInputStream);
        }
    }

    /**
     * <p>Creates a {@link GenericMessage} given a {@link List} of {@link String} where
     * each line represents a {@link GenericSegment}. The method uses the first three
     * characters of line to determine the type of {@link GenericSegment}.</p>
     *
     * @param lines {@link List} of {@link String}s we want as a {@link GenericMessage}
     * @return an object of {@link GenericMessage} if successful
     * @throws IllegalMessageFormat if a {@link GenericSegment} is not valid
     */
    private static GenericMessage build(final List<String> lines)
        throws IllegalMessageFormat {
        logger.finest(() -> String.format("Attempting to create HL7 message. Currently known lines: %s",
            lines.size()));
        // 0 lines file and files that do not start with MSH are not valid HL7
        // messages let's not even try parsing those
        if (lines.size() == 0 || !lines.get(0).startsWith("MSH")) {
            throw new IllegalMessageFormat("Message must begin with a MSH segment");
        }

        GenericMessage message = new GenericMessage();
        MSH msh = new MSH(message).build(lines.get(0));

        message.setFieldDelimiter(msh.getFieldDelimiter());
        message.setComponentDelimiter(msh.getComponentDelimiter());
        message.setSubComponentDelimiter(msh.getSubComponentDelimiter());
        message.setRepeatDelimiter(msh.getRepeatDelimiter());
        message.setEscapeCharacter(msh.getEscapeCharacter());
        message.getSegments().add(msh);

        // If a version specific implementation exists use that builder instead
        ServiceLoader<Message> messageImpls = ServiceLoader.load(Message.class);
        for (Message messageImpl : messageImpls) {
            if (messageImpl.getVersionImpl().equals(msh.getVersionId())) {
                return (GenericMessage) messageImpl.build(lines);
            }
        }

        // No implementation exists for version. Use a catch all builder
        // Note MSH has been processed. Start at 1
        for (int i = 1; i < lines.size(); i++) {
            final String segmentName = lines.get(i).substring(0, 3);

            // search properties for a segment name
            if (properties.getProperty(segmentName) != null) {

                // if property is found leverage reflection for a discrete type
                try {
                    Class<?> c = Class.forName(properties.getProperty(segmentName));
                    Method method = c.getDeclaredMethod("parse", String.class,
                        GenericMessage.class);

                    message.getSegments().add((GenericSegment) method
                        .invoke(null, lines.get(i), message));
                } catch (InvocationTargetException | NoSuchMethodException |
                    IllegalAccessException | ClassNotFoundException e) {
                    // fallback to generic message if something goes wrong
                    logger.log(Level.INFO, String.format("Class not segment " +
                        "name %s not found or implemented.", segmentName), e);

                    GenericSegment segment = new GenericSegment(message)
                        .build(lines.get(i));
                    segment.setSegmentName(segmentName);
                    message.getSegments().add(segment);
                }
            } else {
                GenericSegment segment = new GenericSegment(message)
                    .build(lines.get(i));
                segment.setSegmentName(segmentName);
                message.getSegments().add(segment);
            }
        }

        return message;
    }
}
