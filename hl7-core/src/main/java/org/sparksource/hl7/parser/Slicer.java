package org.sparksource.hl7.parser;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.segments.Segment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <p>Provides a API around message parsing.</p>
 *
 * @author Juan Garcia
 * @since 2017-09-08
 */
public class Slicer {

    final private GenericMessage message;

    public Slicer(final GenericMessage message) {
        this.message = message;
    }

    /**
     * <p>Given a {@link String} finds and returns an {@link Optional} of the first {@link Segment}
     * occurrence.</p>
     *
     * @param name the name of the {@link Segment} that should be returned.
     * @return a {@link Segment} that matches the name provided
     */
    public Optional<Segment> findSegment(final String name) {
        for (Segment segment : message.getSegments()) {
            if (segment.getSegmentName() != null && segment.getSegmentName().equals(name)) {
                return Optional.of(segment);
            }
        }

        return Optional.empty();
    }

    /**
     * <p>Given a {@link String} finds a returns a {@link List} of all {@link Segment}s that match
     * the type.</p>
     *
     * @param name the name of the {@link Segment} that is to be returned i.e. ORU
     * @return the {@link List} of all {@link Segment} items with the provided name
     */
    public List<Segment> findSegmentList(final String name) {
        ArrayList<Segment> segments = new ArrayList<>();

        for (Segment segment : message.getSegments()) {
            if (segment.getSegmentName() != null && segment.getSegmentName().equals(name)) {
                segments.add(segment);
            }
        }

        return segments;
    }

    /**
     * <p>Attempts to identify a field for a given {@link String}. The provided
     * {@link String} should consist of the {@link Segment} name and the position
     * of that value one would like extracted. The method uses the first found
     * {@link Segment} as the line to obtain a field for.</p>
     *
     * @param name the name of the field to be extracted with the character "-"
     *             as the delimiter i.e MSH-9
     * @return an {@link Optional} of the specific field requested
     * @throws InvalidFieldQueryException if a query delimiter is not provided
     */
    public Optional<String> findField(final String name) throws InvalidFieldQueryException {
        if (!name.contains("-")) {
            throw new InvalidFieldQueryException("Delimiter - not present in the query string.");
        }

        String[] qualifier = name.split("-");

        if (qualifier.length == 2) {
            Optional<Segment> segmentOptional = findSegment(qualifier[0]);
            return segmentOptional.map(segment
                -> segment.getSegmentFields().get(Integer.parseInt(qualifier[1])));
        } else {
            throw new InvalidFieldQueryException("More than one qualifier is not supported.");
        }
    }

    public GenericMessage getMessage() {
        return message;
    }
}
