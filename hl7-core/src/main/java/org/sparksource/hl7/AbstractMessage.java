package org.sparksource.hl7;

import org.sparksource.hl7.segments.Segment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>The {@link AbstractMessage} attempts to boot strap the processing of a HL7
 * message by taking care of things that all HL7 messages will need to take into
 * account such as structuring, escaping, etc.</p>
 *
 * @author Juan Garcia
 * @since 2019-04-24
 */
public abstract class AbstractMessage<T> implements Message<T> {

    private List<Segment> segments;

    private Map<String, String> escapeSequences;

    /**
     * <p>Once per HL7 message initialize a {@link Map} that can be used to
     * escape and unescape character sequences that are deemed to be protected.
     * These include the HL7 control characters.</p>
     *
     * <p>This list currently excludes four sequences:</p>
     *
     * <ul>
     * <li>Cxxyy</li>
     * <li>Mxxyyzz</li>
     * <li>Xdd...</li>
     * <li>Zdd...</li>
     * </ul>
     *
     * <p>Determine how to best handle those four character sequences after more
     * reading.</p>
     *
     * @param message the base {@link Message} to construct sequences from
     */
    public void setEscapeSequence(Message message) {
        escapeSequences = new HashMap<>();

        String escapeCharacter = message.getEscapeCharacter();

        escapeSequences.put(escapeCharacter + "E" + escapeCharacter, escapeCharacter);
        escapeSequences.put(escapeCharacter + "F" + escapeCharacter, message.getFieldDelimiter());
        escapeSequences.put(escapeCharacter + "R" + escapeCharacter, message.getRepeatDelimiter());
        escapeSequences.put(escapeCharacter + "S" + escapeCharacter, message.getComponentDelimiter());
        escapeSequences.put(escapeCharacter + "T" + escapeCharacter, message.getSubComponentDelimiter());
    }

    /**
     * <p>All HL7 messages will contain a back {@link List} of the segments at
     * hand. Clients using a {@link Message} need to have a means of altering
     * contents. An implementor should provide the backing {@link List} of all
     * segments.</p>
     *
     * @return the {@link Segment} list that create belong to {@link Message}
     */
    public List<Segment> getSegments() {
        if (segments == null) {
            segments = new ArrayList<>();
        }

        return segments;
    }

    public Map<String, String> getEscapeSequences() {
        return escapeSequences;
    }

    /**
     * <p>Iterates through the {@link Segment} {@link List} creating the HL7
     * {@link String} of the current {@link GenericMessage}.</p>
     *
     * @return the HL7 {@link String} representation of the {@link Segment}
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (Segment segment : getSegments()) {
            if (!builder.toString().equals("")) {
                builder.append("\r");
            }

            builder.append(segment.toString());
        }

        return builder.toString();
    }
}
