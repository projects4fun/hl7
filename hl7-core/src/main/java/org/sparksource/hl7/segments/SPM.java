package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class SPM extends GenericSegment {

    public SPM(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public SPM build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("SPM")) {
            throw new IllegalMessageFormat("Provided line does not start with SPM.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        SPM spm = new SPM(getMessage());
        spm.getSegmentFields().addAll(Arrays.asList(fields));
        return spm;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static SPM parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("SPM")) {
            throw new IllegalMessageFormat("Provided line does not start with SPM.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        SPM SPM = new SPM(message);

        for (int i = 0; i < fields.length; i++) {
            SPM.setFieldValue(i, fields[i]);
        }

        return SPM;
    }
}
