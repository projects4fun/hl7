package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RXC extends GenericSegment {

    public RXC(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RXC build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXC")) {
            throw new IllegalMessageFormat("Provided line does not start with RXC.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RXC rxc = new RXC(getMessage());
        rxc.getSegmentFields().addAll(Arrays.asList(fields));
        return rxc;
    }


    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RXC parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXC")) {
            throw new IllegalMessageFormat("Provided line does not start with RXC.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RXC RXC = new RXC(message);

        for (int i = 0; i < fields.length; i++) {
            RXC.setFieldValue(i, fields[i]);
        }

        return RXC;
    }
}
