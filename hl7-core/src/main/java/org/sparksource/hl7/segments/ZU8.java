package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU8 extends GenericSegment {

    public ZU8(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU8 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU8")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU8.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU8 zu8 = new ZU8(getMessage());
        zu8.getSegmentFields().addAll(Arrays.asList(fields));
        return zu8;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU8 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU8")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU8.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU8 ZU8 = new ZU8(message);

        for (int i = 0; i < fields.length; i++) {
            ZU8.setFieldValue(i, fields[i]);
        }

        return ZU8;
    }
}
