package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Optional;

/**
 * <p>An API for building and interacting {@link PID} segment.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-12
 */
public class PID extends GenericSegment implements FieldAccessor<PID, PID.FieldName> {

    public enum FieldName {
        SET_ID(1), PATIENT_EXTERNAL_ID(2), PATIENT_INTERNAL_ID(3),
        ALTERNATE_PATIENT_ID(4), PATIENT_NAME(5),
        MOTHER_MAIDEN_NAME(6), DATE_OF_BIRTH(7),
        SEX(8), PATIENT_ALIAS(9), RACE(10),
        PATIENT_ADDRESS(11), COUNTRY_CODE(12),
        HOME_PHONE_NUMBER(13), WORK_PHONE_NUMBER(14),
        PRIMARY_LANGUAGE(15), MARITAL_STATUS(16),
        RELIGION(17), PATIENT_ACCOUNT_NUMBER(18),
        SSN(19), DRIVERS_LICENSE_NUMBER(20),
        MOTHERS_IDENTIFIER(21), ETHNIC_GROUP(22),
        BIRTH_PLACE(23), MULTIPLE_BIRTH_INDICATOR(24),
        BIRTH_ORDER(25), CITIZENSHIP(26),
        VETERANS_MILITARY_STATUS(27), NATIONALITY(28),
        PATIENT_DEATH_DATETIME(29), PATIENT_DEATH_INDICATOR(30);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public PID(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public PID build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PID")) {
            throw new IllegalMessageFormat("Provided line does not start with PID.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            setFieldValue(i, fields[i]);
        }

        return this;
    }


    /**
     * @param line    the line to be parsed into a {@link PID}
     * @param message the {@link GenericMessage} containing delimiter information
     * @return an instance of a {@link PID}
     * @throws IllegalMessageFormat if the line is null;
     *                              if the line is empty;
     *                              if the line does not start with PID
     */
    public static PID parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PID")) {
            throw new IllegalMessageFormat("Provided line does not start with PID.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        PID pid = new PID(message);

        for (int i = 0; i < fields.length; i++) {
            pid.setFieldValue(i, fields[i]);
        }

        return pid;
    }

    public String getSetId() {
        return getFieldValue(FieldName.SET_ID).orElse("");
    }

    public void setSetId(final String setId) {
        setFieldValue(FieldName.SET_ID.getValue(), setId);
    }

    public String getPatientExternalId() {
        return getFieldValue(FieldName.PATIENT_EXTERNAL_ID).orElse("");
    }

    public void setPatientExternalId(final String patientExternalId) {
        setFieldValue(FieldName.PATIENT_EXTERNAL_ID.getValue(), patientExternalId);
    }

    public String getPatientInternalId() {
        return getFieldValue(FieldName.PATIENT_INTERNAL_ID).orElse("");
    }

    public void setPatientInternalId(String patientInternalId) {
        setFieldValue(FieldName.PATIENT_INTERNAL_ID.getValue(), patientInternalId);
    }

    public String getAlternatePatientId() {
        return getFieldValue(FieldName.ALTERNATE_PATIENT_ID).orElse("");
    }

    public void setAlternatePatientId(String alternatePatientId) {
        setFieldValue(FieldName.ALTERNATE_PATIENT_ID.getValue(), alternatePatientId);
    }

    public String getPatientName() {
        return getFieldValue(FieldName.PATIENT_NAME).orElse("");
    }

    public void setPatientName(String patientName) {
        setFieldValue(FieldName.PATIENT_NAME.getValue(), patientName);
    }

    public String getMotherMaidenName() {
        return getFieldValue(FieldName.MOTHER_MAIDEN_NAME).orElse("");
    }

    public void setMotherMaidenName(String motherMaidenName) {
        setFieldValue(FieldName.MOTHER_MAIDEN_NAME.getValue(), motherMaidenName);
    }

    public String getDateOfBirth() {
        return getFieldValue(FieldName.DATE_OF_BIRTH).orElse("");
    }

    public void setDateOfBirth(String dateOfBirth) {
        setFieldValue(FieldName.DATE_OF_BIRTH.getValue(), dateOfBirth);
    }

    public String getSex() {
        return getFieldValue(FieldName.SEX).orElse("");
    }

    public void setSex(String sex) {

        setFieldValue(FieldName.SEX.getValue(), sex);
    }

    public String getPatientAlias() {
        return getFieldValue(FieldName.PATIENT_ALIAS).orElse("");
    }

    public void setPatientAlias(String patientAlias) {
        setFieldValue(FieldName.PATIENT_ALIAS.getValue(), patientAlias);
    }

    public String getRace() {
        return getFieldValue(FieldName.RACE).orElse("");
    }

    public void setRace(String race) {

        setFieldValue(FieldName.RACE.getValue(), race);
    }

    public String getPatientAddress() {
        return getFieldValue(FieldName.PATIENT_ADDRESS).orElse("");
    }

    public void setPatientAddress(String patientAddress) {
        setFieldValue(FieldName.PATIENT_ADDRESS.getValue(), patientAddress);
    }

    public String getCountryCode() {
        return getFieldValue(FieldName.COUNTRY_CODE).orElse("");
    }

    public void setCountryCode(String countryCode) {
        setFieldValue(FieldName.COUNTRY_CODE.getValue(), countryCode);
    }

    public String getHomePhone() {
        return getFieldValue(FieldName.HOME_PHONE_NUMBER).orElse("");
    }

    public void setHomePhone(String homePhone) {
        setFieldValue(FieldName.HOME_PHONE_NUMBER.getValue(), homePhone);
    }

    public String getWorkPhone() {
        return getFieldValue(FieldName.WORK_PHONE_NUMBER).orElse("");
    }

    public void setWorkPhone(String workPhone) {
        setFieldValue(FieldName.WORK_PHONE_NUMBER.getValue(), workPhone);
    }

    public String getPrimaryLanguage() {
        return getFieldValue(FieldName.PRIMARY_LANGUAGE).orElse("");
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        setFieldValue(FieldName.PRIMARY_LANGUAGE.getValue(), primaryLanguage);
    }

    public String getMaritalStatus() {
        return getFieldValue(FieldName.MARITAL_STATUS).orElse("");
    }

    public void setMaritalStatus(String maritalStatus) {
        setFieldValue(FieldName.MARITAL_STATUS.getValue(), maritalStatus);
    }

    public String getReligion() {
        return getFieldValue(FieldName.RELIGION).orElse("");
    }

    public void setReligion(String religion) {
        setFieldValue(FieldName.RELIGION.getValue(), religion);
    }

    public String getPatientAccountNumber() {
        return getFieldValue(FieldName.PATIENT_ACCOUNT_NUMBER).orElse("");
    }

    public void setPatientAccountNumber(String patientAccountNumber) {
        setFieldValue(FieldName.PATIENT_ACCOUNT_NUMBER.getValue(), patientAccountNumber);
    }

    public String getSsn() {
        return getFieldValue(FieldName.SSN).orElse("");
    }

    public void setSsn(String ssn) {
        setFieldValue(FieldName.SSN.getValue(), ssn);
    }

    public String getDriversLicenseNumber() {
        return getFieldValue(FieldName.DRIVERS_LICENSE_NUMBER).orElse("");
    }

    public void setDriversLicenseNumber(String driversLicenseNumber) {
        setFieldValue(FieldName.DRIVERS_LICENSE_NUMBER.getValue(), driversLicenseNumber);
    }

    public String getMothersIdentifier() {
        return getFieldValue(FieldName.MOTHERS_IDENTIFIER).orElse("");
    }

    public void setMothersIdentifier(String mothersIdentifier) {
        setFieldValue(FieldName.MOTHERS_IDENTIFIER.getValue(), mothersIdentifier);
    }

    public String getEthnicGroup() {
        return getFieldValue(FieldName.ETHNIC_GROUP).orElse("");
    }

    public void setEthnicGroup(String ethnicGroup) {
        setFieldValue(FieldName.ETHNIC_GROUP.getValue(), ethnicGroup);
    }

    public String getBirthPlace() {
        return getFieldValue(FieldName.BIRTH_PLACE).orElse("");
    }

    public void setBirthPlace(String birthPlace) {
        setFieldValue(FieldName.BIRTH_PLACE.getValue(), birthPlace);
    }

    public String getMultipleBirthIndicator() {
        return getFieldValue(FieldName.MULTIPLE_BIRTH_INDICATOR).orElse("");
    }

    public void setMultipleBirthIndicator(String multipleBirthIndicator) {
        setFieldValue(FieldName.MULTIPLE_BIRTH_INDICATOR.getValue(), multipleBirthIndicator);
    }

    public String getBirthOrder() {
        return getFieldValue(FieldName.BIRTH_ORDER).orElse("");
    }

    public void setBirthOrder(String birthOrder) {
        setFieldValue(FieldName.BIRTH_ORDER.getValue(), birthOrder);
    }

    public String getCitizenship() {
        return getFieldValue(FieldName.CITIZENSHIP).orElse("");
    }

    public void setCitizenship(String citizenship) {
        setFieldValue(FieldName.CITIZENSHIP.getValue(), citizenship);
    }

    public String getVeteransMilitaryStatus() {
        return getFieldValue(FieldName.VETERANS_MILITARY_STATUS).orElse("");
    }

    public void setVeteransMilitaryStatus(String veteransMilitaryStatus) {
        setFieldValue(FieldName.VETERANS_MILITARY_STATUS.getValue(), veteransMilitaryStatus);
    }

    public String getNationality() {
        return getFieldValue(FieldName.NATIONALITY).orElse("");
    }

    public void setNationality(String nationality) {
        setFieldValue(FieldName.NATIONALITY.getValue(), nationality);
    }

    public String getPatientDeathDatetime() {
        return getFieldValue(FieldName.PATIENT_DEATH_DATETIME).orElse("");
    }

    public void setPatientDeathDatetime(String patientDeathDatetime) {
        setFieldValue(FieldName.PATIENT_DEATH_DATETIME.getValue(), patientDeathDatetime);
    }

    public String getPatientDeathIndicator() {
        return getFieldValue(FieldName.PATIENT_DEATH_INDICATOR).orElse("");
    }

    public void setPatientDeathIndicator(String patientDeathIndicator) {
        setFieldValue(FieldName.PATIENT_DEATH_INDICATOR.getValue(), patientDeathIndicator);
    }

    /**
     * <p>Provides a means to define {@link String} values for a particular
     * field location.</p>
     *
     * @param fieldName the location a value belongs in
     * @param value     the content to be placed in a defined segment
     * @return this object for chaining sets
     */
    @Override
    public PID setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value, value);
        return this;
    }

    /**
     * <p>Provides a means to query for a particular {@link FieldName}.</p>
     *
     * @param fieldName the value to be retrieve
     * @return an {@link Optional} of the content identified
     */
    @Override
    public Optional<String> getFieldValue(FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
