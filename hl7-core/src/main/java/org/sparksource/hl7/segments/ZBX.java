package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZBX extends GenericSegment {

    public ZBX(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZBX build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZBX")) {
            throw new IllegalMessageFormat("Provided line does not start with ZBX.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZBX zbx = new ZBX(getMessage());
        zbx.getSegmentFields().addAll(Arrays.asList(fields));
        return zbx;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZBX parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZBX")) {
            throw new IllegalMessageFormat("Provided line does not start with ZBX.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZBX ZBX = new ZBX(message);

        for (int i = 0; i < fields.length; i++) {
            ZBX.setFieldValue(i, fields[i]);
        }

        return ZBX;
    }
}
