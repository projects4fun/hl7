package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class FT1 extends GenericSegment {

    public FT1(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public FT1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("FT1")) {
            throw new IllegalMessageFormat("Provided line does not start with FT1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        FT1 ft1 = new FT1(getMessage());
        ft1.getSegmentFields().addAll(Arrays.asList(fields));
        return ft1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static FT1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("FT1")) {
            throw new IllegalMessageFormat("Provided line does not start with FT1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        FT1 ft1 = new FT1(message);

        for (int i = 0; i < fields.length; i++) {
            ft1.setFieldValue(i, fields[i]);
        }

        return ft1;
    }
}
