package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZCN extends GenericSegment {

    public ZCN(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZCN build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZCN")) {
            throw new IllegalMessageFormat("Provided line does not start with ZCN.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZCN zcn = new ZCN(getMessage());
        zcn.getSegmentFields().addAll(Arrays.asList(fields));
        return zcn;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZCN parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZCN")) {
            throw new IllegalMessageFormat("Provided line does not start with ZCN.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZCN ZCN = new ZCN(message);

        for (int i = 0; i < fields.length; i++) {
            ZCN.setFieldValue(i, fields[i]);
        }

        return ZCN;
    }
}
