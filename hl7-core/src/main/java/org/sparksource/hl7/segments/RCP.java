package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Juan Garcia
 * @since 2018-01-09
 */
public class RCP extends GenericSegment implements FieldAccessor<RCP, RCP.FieldName> {

    public enum FieldName {
        QUERY_PRIORITY(1), QUANTITY_LIMITED_REQUEST(2), RESPONSE_MODALITY(3),
        EXECUTION_AND_DELIVERY_TIME(4), MODIFY_INDICATOR(5), SORT_BY_FIELD(6),
        SEGMENT_GROUP_INCLUSION(7);

        private int value;

        FieldName(final int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public RCP(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RCP build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RCP")) {
            throw new IllegalMessageFormat("Provided line does not start with RCP.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RCP rcp = new RCP(getMessage());
        rcp.getSegmentFields().addAll(Arrays.asList(fields));
        return rcp;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RCP parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RCP")) {
            throw new IllegalMessageFormat("Provided line does not start with RCP.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RCP RCP = new RCP(message);

        for (int i = 0; i < fields.length; i++) {
            RCP.setFieldValue(i, fields[i]);
        }

        return RCP;
    }

    /**
     * <p>Provides a means to define {@link String} values for a particular
     * field location.</p>
     *
     * @param fieldName the location a value belongs in
     * @param value     the content to be placed in a defined segment
     * @return this object for chaining sets
     */
    @Override
    public RCP setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value(), value);
        return this;
    }

    /**
     * <p>Provides a means to query for a particular {@link FieldName}.</p>
     *
     * @param fieldName the value to be retrieve
     * @return an {@link Optional} of the content identified
     */
    @Override
    public Optional<String> getFieldValue(final FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
