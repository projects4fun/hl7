package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RXO extends GenericSegment {

    public RXO(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RXO build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXO")) {
            throw new IllegalMessageFormat("Provided line does not start with RXO.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RXO rxo = new RXO(getMessage());
        rxo.getSegmentFields().addAll(Arrays.asList(fields));
        return rxo;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RXO parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXO")) {
            throw new IllegalMessageFormat("Provided line does not start with RXO.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RXO RXO = new RXO(message);

        for (int i = 0; i < fields.length; i++) {
            RXO.setFieldValue(i, fields[i]);
        }

        return RXO;
    }
}
