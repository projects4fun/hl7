package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class AIS extends GenericSegment {

    public AIS(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public AIS build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AIS")) {
            throw new IllegalMessageFormat("Provided line does not start with AIS.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        AIS ais = new AIS(getMessage());
        ais.getSegmentFields().addAll(Arrays.asList(fields));
        return ais;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static AIS parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AIS")) {
            throw new IllegalMessageFormat("Provided line does not start with AIS.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        AIS ais = new AIS(message);

        for (int i = 0; i < fields.length; i++) {
            ais.setFieldValue(i, fields[i]);
        }

        return ais;
    }
}
