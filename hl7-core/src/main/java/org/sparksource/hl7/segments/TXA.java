package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class TXA extends GenericSegment {

    public TXA(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public TXA build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("TXA")) {
            throw new IllegalMessageFormat("Provided line does not start with TXA.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        TXA txa = new TXA(getMessage());
        txa.getSegmentFields().addAll(Arrays.asList(fields));
        return txa;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static TXA parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("TXA")) {
            throw new IllegalMessageFormat("Provided line does not start with TXA.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        TXA TXA = new TXA(message);

        for (int i = 0; i < fields.length; i++) {
            TXA.setFieldValue(i, fields[i]);
        }

        return TXA;
    }
}
