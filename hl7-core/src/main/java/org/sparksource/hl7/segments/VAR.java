package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class VAR extends GenericSegment {

    public VAR(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public VAR build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("VAR")) {
            throw new IllegalMessageFormat("Provided line does not start with VAR.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        VAR var = new VAR(getMessage());
        var.getSegmentFields().addAll(Arrays.asList(fields));
        return var;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static VAR parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("VAR")) {
            throw new IllegalMessageFormat("Provided line does not start with VAR.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        VAR VAR = new VAR(message);

        for (int i = 0; i < fields.length; i++) {
            VAR.setFieldValue(i, fields[i]);
        }

        return VAR;
    }
}
