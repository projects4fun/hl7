package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class TQ1 extends GenericSegment {

    public TQ1(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public TQ1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("TQ1")) {
            throw new IllegalMessageFormat("Provided line does not start with TQ1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        TQ1 tq1 = new TQ1(getMessage());
        tq1.getSegmentFields().addAll(Arrays.asList(fields));
        return tq1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static TQ1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("TQ1")) {
            throw new IllegalMessageFormat("Provided line does not start with TQ1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        TQ1 TQ1 = new TQ1(message);

        for (int i = 0; i < fields.length; i++) {
            TQ1.setFieldValue(i, fields[i]);
        }

        return TQ1;
    }
}
