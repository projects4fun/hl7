package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU2 extends GenericSegment {

    public ZU2(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU2 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU2")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU2.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU2 zu2 = new ZU2(getMessage());
        zu2.getSegmentFields().addAll(Arrays.asList(fields));
        return zu2;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU2 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU2")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU2.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU2 ZU2 = new ZU2(message);

        for (int i = 0; i < fields.length; i++) {
            ZU2.setFieldValue(i, fields[i]);
        }

        return ZU2;
    }
}
