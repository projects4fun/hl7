package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU3 extends GenericSegment {

    public ZU3(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU3 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU3")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU3.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU3 ZU3 = new ZU3(getMessage());
        ZU3.getSegmentFields().addAll(Arrays.asList(fields));
        return ZU3;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU3 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU3")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU3.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU3 ZU3 = new ZU3(message);

        for (int i = 0; i < fields.length; i++) {
            ZU3.setFieldValue(i, fields[i]);
        }

        return ZU3;
    }
}
