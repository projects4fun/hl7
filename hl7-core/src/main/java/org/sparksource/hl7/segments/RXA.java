package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RXA extends GenericSegment implements FieldAccessor<RXA, RXA.FieldName> {

    public enum FieldName {
        GIVE_SUB_ID_COUNTER(1),
        ADMINISTRATION_SUB_ID_COUNTER(2),
        DATE_TIME_START_OF_ADMINISTRATION(3),
        DATE_TIME_END_OF_ADMINISTRATION(4),
        ADMINISTERED_CODE(5),
        ADMINISTERED_AMOUNT(6),
        ADMINISTERED_UNITS(7),
        ADMINISTERED_DOSAGE_FORM(8),
        ADMINISTRATION_NOTES(9),
        ADMINISTERING_PROVIDER(10),
        ADMINISTERED_AT_LOCATION(11),
        ADMINISTERED_PER(12),
        ADMINISTERED_STRENGTH(13),
        ADMINISTERED_STRENGTH_UNITS(14),
        SUBSTANCE_LOT_NUMBER(15),
        SUBSTANCE_EXPIRATION_DATE(16),
        SUBSTANCE_MANUFACTURER_NAME(17),
        SUBSTANCE_TREATMENT_REFUSAL_REASON(18),
        INDICATION(19),
        COMPLETION_STATUS(20),
        ACTION_CODE_RXA(21),
        SYSTEM_ENTRY_DATE_TIME(22),
        ADMINISTERED_DRUG_STRENGTH_VOLUME(23),
        ADMINISTERED_DRUG_STRENGTH_VOLUME_UNITS(24),
        ADMINISTERED_BARCODE_IDENTIFIER(25),
        PHARMACY_ORDER_TYPE(26);

        final private int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public RXA(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RXA build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXA")) {
            throw new IllegalMessageFormat("Provided line does not start with RXA.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RXA rxa = new RXA(getMessage());
        rxa.getSegmentFields().addAll(Arrays.asList(fields));
        return rxa;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RXA parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXA")) {
            throw new IllegalMessageFormat("Provided line does not start with RXA.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RXA RXA = new RXA(message);

        for (int i = 0; i < fields.length; i++) {
            RXA.setFieldValue(i, fields[i]);
        }

        return RXA;
    }

    /**
     * <p>Allows for values to be defined based on the declared enums of a
     * segment.</p>
     *
     * @param fieldName the field name that should be assigned
     * @param value     the value to be assigned
     * @return the current message for message chains
     */
    @Override
    public RXA setFieldValue(FieldName fieldName, String value) {
        setFieldValue(fieldName.value, value);
        return this;
    }

    /**
     * <p>Allows for a query on a field value</p>
     *
     * @param fieldName the field name being queried
     * @return an optional of the queried content
     */
    @Override
    public Optional<String> getFieldValue(FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
