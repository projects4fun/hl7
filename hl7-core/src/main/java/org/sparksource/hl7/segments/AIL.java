package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class AIL extends GenericSegment {

    public AIL(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public AIL build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AIL")) {
            throw new IllegalMessageFormat("Provided line does not start with AIL.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        AIL ail = new AIL(getMessage());
        ail.getSegmentFields().addAll(Arrays.asList(fields));
        return ail;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static AIL parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AIL")) {
            throw new IllegalMessageFormat("Provided line does not start with AIL.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        AIL ail = new AIL(message);

        for (int i = 0; i < fields.length; i++) {
            ail.setFieldValue(i, fields[i]);
        }

        return ail;
    }
}
