package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * <p>Provides building and querying a {@link QPD} capabilities.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class QPD extends GenericSegment implements FieldAccessor<QPD, QPD.FieldName> {

    public enum FieldName {
        MESSAGE_QUERY_NAME(1), // Z34^Request Complete Immunization History^CDCPHINVS
        QUERY_TAG(2),
        ID_LIST(3), // PID-3: Patient Identifier List
        NAME(4), // PID-5: Patient Name
        MAIDEN_MOTHER_NAME(5), // PID-6: Mother’s maiden name
        DATE_OF_BIRTH(6), // PID-7: Patient date of birth
        SEX(7), // PID-8: Patient sex
        ADDRESS(8), // PID-11: Patient Address
        HOME_PHONE(9), // PID-13: Patient home phone
        MULTIPLE_BIRTH_INDICATOR(10), // PID-24: Patient multiple birth indicator
        BIRTH_ORDER(11), // PID-25: Patient birth order
        LAST_UPDATE_DATE(12), // PID-33: Patient last update date
        LAST_UPDATE_FACILITY(13); // PID-34: Patient last update facility

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    /**
     * <p>Default constructor.</p>
     *
     * @param message a message to help determine how a {@link QPD} should be
     *                transformed to a {@link String}
     */
    public QPD(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public QPD build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("QPD")) {
            throw new IllegalMessageFormat("Provided line does not start with QPD.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage()
            .getFieldDelimiter()));
        QPD qpd = new QPD(getMessage());
        qpd.getSegmentFields().addAll(Arrays.asList(fields));
        return qpd;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static QPD parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("QPD")) {
            throw new IllegalMessageFormat("Provided line does not start with QPD.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        QPD QPD = new QPD(message);

        for (int i = 0; i < fields.length; i++) {
            QPD.setFieldValue(i, fields[i]);
        }

        return QPD;
    }

    /**
     * <p>Provides a means to define {@link String} values for a particular
     * field location.</p>
     *
     * @param fieldName the location a value belongs in
     * @param value     the content to be placed in a defined segment
     * @return this object for chaining sets
     */
    @Override
    public QPD setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value(), value);
        return this;
    }

    /**
     * <p>Provides a means to query for a particular {@link FieldName}.</p>
     *
     * @param fieldName the value to be retrieve
     * @return an {@link Optional} of the content identified
     */
    @Override
    public Optional<String> getFieldValue(final FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
