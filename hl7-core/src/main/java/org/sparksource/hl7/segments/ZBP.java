package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZBP extends GenericSegment {

    public ZBP(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZBP build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZBP")) {
            throw new IllegalMessageFormat("Provided line does not start with ZBP.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZBP zbp = new ZBP(getMessage());
        zbp.getSegmentFields().addAll(Arrays.asList(fields));
        return zbp;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZBP parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZBP")) {
            throw new IllegalMessageFormat("Provided line does not start with ZBP.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZBP ZBP = new ZBP(message);

        for (int i = 0; i < fields.length; i++) {
            ZBP.setFieldValue(i, fields[i]);
        }

        return ZBP;
    }
}
