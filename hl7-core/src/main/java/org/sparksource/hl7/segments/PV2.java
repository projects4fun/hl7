package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class PV2 extends GenericSegment {

    public PV2(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public PV2 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PV2")) {
            throw new IllegalMessageFormat("Provided line does not start with PV2.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        PV2 pv2 = new PV2(getMessage());
        pv2.getSegmentFields().addAll(Arrays.asList(fields));
        return pv2;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static PV2 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PV2")) {
            throw new IllegalMessageFormat("Provided line does not start with PV2.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        PV2 PV2 = new PV2(message);

        for (int i = 0; i < fields.length; i++) {
            PV2.setFieldValue(i, fields[i]);
        }

        return PV2;
    }
}
