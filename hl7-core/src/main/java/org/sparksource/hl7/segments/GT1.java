package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-16
 */
public class GT1 extends GenericSegment {

    public GT1(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public GT1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("GT1")) {
            throw new IllegalMessageFormat("Provided line does not start with GT1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));

        GT1 gt1 = new GT1(getMessage());
        gt1.getSegmentFields().addAll(Arrays.asList(fields));
        return gt1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static GT1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("GT1")) {
            throw new IllegalMessageFormat("Provided line does not start with GT1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        GT1 GT1 = new GT1(message);

        for (int i = 0; i < fields.length; i++) {
            GT1.setFieldValue(i, fields[i]);
        }

        return GT1;
    }
}
