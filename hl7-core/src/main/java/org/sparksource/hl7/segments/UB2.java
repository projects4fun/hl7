package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class UB2 extends GenericSegment {

    public UB2(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public UB2 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("UB2")) {
            throw new IllegalMessageFormat("Provided line does not start with UB2.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        UB2 ub2 = new UB2(getMessage());
        ub2.getSegmentFields().addAll(Arrays.asList(fields));
        return ub2;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static UB2 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("UB2")) {
            throw new IllegalMessageFormat("Provided line does not start with UB2.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        UB2 UB2 = new UB2(message);

        for (int i = 0; i < fields.length; i++) {
            UB2.setFieldValue(i, fields[i]);
        }

        return UB2;
    }
}
