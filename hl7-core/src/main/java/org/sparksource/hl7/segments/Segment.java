package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>A base type representing a possible item in a HL7 message. All discrete
 * items MSH, PID, PV1, etc extend {@link Segment}.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-11
 */
public abstract class Segment {

    private List<String> segmentFields;
    final private GenericMessage message;

    public Segment(final GenericMessage message) {
        this.message = message;
    }

    /**
     * <p>The name for a given {@link Segment}. The {@link GenericSegment} is
     * the class to have the most use out of this requirement. The intention is
     * to label a {@link Segment} and allow clients to know of the underlying
     * {@link Segment} being provided when a concrete type can not be determined
     * such as a custom ZXX type segment.</p>
     *
     * <p>Segment names are always three characters.</p>
     *
     * @return the name of the {@link Segment}
     */
    public abstract String getSegmentName();

    /**
     * <p>Every {@link Segment} contains a fields under it. A {@link List} acts
     * as the current means of accessing said fields. This list operations on
     * {@link String}s rather than discrete data types for the time being. To
     * support addition, removal, and modification the underlying {@link List}
     * should be accessible to clients extending on {@link Segment}. This method
     * will never return null - should the backing variable be null a new
     * {@link ArrayList} will be assigned and returned.</p>
     *
     * @return the underlying {@link List} of fields for a {@link Segment}
     */
    public List<String> getSegmentFields() {
        if (segmentFields == null) {
            segmentFields = new ArrayList<>();
        }

        return segmentFields;
    }

    /**
     * <p>Exposes the underlying {@link Message} to clients. The underlying
     * message exposes items such as the encoding character that are necessary
     * to parse and create entire portions of the {@link Message}</p>
     *
     * @return the underlying {@link Message}
     */
    public GenericMessage getMessage() {
        return message;
    }
}
