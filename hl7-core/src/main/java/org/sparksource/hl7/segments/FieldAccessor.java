package org.sparksource.hl7.segments;

import java.util.Optional;

/**
 * <p>Defines a standard means of accessing and setting data elements from
 * {@link Segment} entries in a consistent format even if concrete accessors may
 * not have been defined.</p>
 *
 * @author Juan Garcia
 * @since 2018-01-16
 */
public interface FieldAccessor<E, T> {

    /**
     * <p>Allows for values to be defined based on the declared enums of a
     * segment.</p>
     *
     * @param fieldName the field name that should be assigned
     * @param value     the value to be assigned
     * @return the current message for message chains
     */
    E setFieldValue(final T fieldName, final String value);

    /**
     * <p>Allows for a query on a field value</p>
     *
     * @param fieldName the field name being queried
     * @return an optional of the queried content
     */
    Optional<String> getFieldValue(final T fieldName);
}
