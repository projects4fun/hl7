package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU1 extends GenericSegment {

    public ZU1(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU1")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU1 zu1 = new ZU1(getMessage());
        zu1.getSegmentFields().addAll(Arrays.asList(fields));
        return zu1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU1")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU1 ZU1 = new ZU1(message);

        for (int i = 0; i < fields.length; i++) {
            ZU1.setFieldValue(i, fields[i]);
        }

        return ZU1;
    }
}
