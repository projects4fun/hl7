package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class QAK extends GenericSegment implements FieldAccessor<QAK, QAK.FieldName> {

    public enum FieldName {
        /**
         * <p>This field may be valued by the initiating system to identify the
         * query, and may be used to match response messages to the originating
         * query. If it is valued, the responding system is required to echo it
         * back as the first field in the query acknowledgment segment (QAK).
         * This field differs from MSA-2-message control ID in that its value
         * remains constant for each message (i.e., all continuation messages)
         * associated with the query, whereas MSA-2-GenericMessage control ID may vary
         * with each continuation message, since it is associated with each
         * individual message, not the query as a whole. QAK-1-Query tag is not
         * conditional on the presence of the QRD-1-Query ID field in the
         * original mode queries; in the original mode queries QAK-1-Query tag
         * is not used.</p>
         */
        QUERY_TAG(1),
        /**
         * <p>This field allows the responding system to return a precise
         * response status. It is especially useful in the case where no data is
         * found that matches the query parameters, but where there is also no
         * error. Refer to HL7 Table 0208 - Query Response Status in Chapter 2C,
         * Code Tables, for valid values.</p>
         */
        QUERY_RESPONSE_STATUS(2),
        /**
         * <p>This field contains the name of the query. These names are
         * assigned by the function-specific chapters of this specification.
         * Site-specific event replay query names begin with the letter "Z."
         * Refer to User defined table 0471 - Query name in Chapter 2C, Code
         * Tables, for suggested values.</p>
         */
        MESSAGE_QUERY_NAME(3),
        /**
         * <p>This field, when used, contains the total number of records found
         * by the Server that matched the query. For tabular responses, this is
         * the number of rows found. For other response types, the Query Profile
         * defines the meaning of a "hit."</p>
         */
        HIT_COUNT_TOTAL(4),
        /**
         * <p>This field, when used, contains the total number of matching
         * records that the Server sent in the current response. Where the
         * continuation protocol is used to transmit the response in partial
         * installments, this number will differ from the value sent in
         * QAK-4-Hit count total.</p>
         */
        PAYLOAD(5),
        /**
         * <p>This field, when used, contains the number of matching records
         * found by the Server that have yet to be sent. It is only meaningful
         * when the Server uses the continuation protocol to transmit partial
         * responses.</p>
         */
        HITS_REMAINING(6);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public QAK(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public QAK build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("QAK")) {
            throw new IllegalMessageFormat("Provided line does not start with QAK.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        QAK qak = new QAK(getMessage());
        qak.getSegmentFields().addAll(Arrays.asList(fields));
        return qak;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static QAK parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("QAK")) {
            throw new IllegalMessageFormat("Provided line does not start with QAK.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        QAK QAK = new QAK(message);

        for (int i = 0; i < fields.length; i++) {
            QAK.setFieldValue(i, fields[i]);
        }

        return QAK;
    }

    /**
     * <p>Provides a means to define {@link String} values for a particular
     * field location.</p>
     *
     * @param fieldName the location a value belongs in
     * @param value     the content to be placed in a defined segment
     * @return this object for chaining sets
     */
    @Override
    public QAK setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value(), value);
        return this;
    }

    /**
     * <p>Provides a means to query for a particular {@link FieldName}.</p>
     *
     * @param fieldName the value to be retrieve
     * @return an {@link Optional} of the content identified
     */
    @Override
    public Optional<String> getFieldValue(final FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
