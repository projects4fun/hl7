package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU4 extends GenericSegment {

    public ZU4(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU4 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU4")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU4.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU4 zu4 = new ZU4(getMessage());
        zu4.getSegmentFields().addAll(Arrays.asList(fields));
        return zu4;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU4 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU4")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU4.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU4 ZU4 = new ZU4(message);

        for (int i = 0; i < fields.length; i++) {
            ZU4.setFieldValue(i, fields[i]);
        }

        return ZU4;
    }
}
