package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class SCH extends GenericSegment {

    public SCH(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public SCH build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("SCH")) {
            throw new IllegalMessageFormat("Provided line does not start with SCH.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        SCH sch = new SCH(getMessage());
        sch.getSegmentFields().addAll(Arrays.asList(fields));
        return sch;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static SCH parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("SCH")) {
            throw new IllegalMessageFormat("Provided line does not start with SCH.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        SCH SCH = new SCH(message);

        for (int i = 0; i < fields.length; i++) {
            SCH.setFieldValue(i, fields[i]);
        }

        return SCH;
    }
}
