package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZAL extends GenericSegment {

    public ZAL(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZAL build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZAL")) {
            throw new IllegalMessageFormat("Provided line does not start with ZAL.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZAL zal = new ZAL(getMessage());
        zal.getSegmentFields().addAll(Arrays.asList(fields));
        return zal;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZAL parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZAL")) {
            throw new IllegalMessageFormat("Provided line does not start with ZAL.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZAL ZAL = new ZAL(message);

        for (int i = 0; i < fields.length; i++) {
            ZAL.setFieldValue(i, fields[i]);
        }

        return ZAL;
    }
}
