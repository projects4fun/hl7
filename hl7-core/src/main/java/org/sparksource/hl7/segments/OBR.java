package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-16
 */
public class OBR extends GenericSegment {

    public OBR(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public OBR build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("OBR")) {
            throw new IllegalMessageFormat("Provided line does not start with OBR.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        OBR obr = new OBR(getMessage());
        obr.getSegmentFields().addAll(Arrays.asList(fields));
        return obr;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static OBR parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("OBR")) {
            throw new IllegalMessageFormat("Provided line does not start with OBR.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        OBR obr = new OBR(message);

        for (int i = 0; i < fields.length; i++) {
            obr.setFieldValue(i, fields[i]);
        }

        return obr;
    }
}
