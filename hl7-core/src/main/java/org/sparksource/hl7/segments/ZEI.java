package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZEI extends GenericSegment {

    public ZEI(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZEI build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZEI")) {
            throw new IllegalMessageFormat("Provided line does not start with ZEI.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZEI zei = new ZEI(getMessage());
        zei.getSegmentFields().addAll(Arrays.asList(fields));
        return zei;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZEI parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZEI")) {
            throw new IllegalMessageFormat("Provided line does not start with ZEI.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZEI ZEI = new ZEI(message);

        for (int i = 0; i < fields.length; i++) {
            ZEI.setFieldValue(i, fields[i]);
        }

        return ZEI;
    }
}
