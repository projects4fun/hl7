package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class IN3 extends GenericSegment {

    public IN3(final GenericMessage message) {
        super(message);
        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public IN3 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("IN3")) {
            throw new IllegalMessageFormat("Provided line does not start with IN3.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        IN3 in3 = new IN3(getMessage());
        in3.getSegmentFields().addAll(Arrays.asList(fields));
        return in3;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static IN3 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("IN3")) {
            throw new IllegalMessageFormat("Provided line does not start with IN3.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        IN3 IN3 = new IN3(message);

        for (int i = 0; i < fields.length; i++) {
            IN3.setFieldValue(i, fields[i]);
        }

        return IN3;
    }
}
