package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RXE extends GenericSegment {

    public RXE(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RXE build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXE")) {
            throw new IllegalMessageFormat("Provided line does not start with RXE.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RXE rxe = new RXE(getMessage());
        rxe.getSegmentFields().addAll(Arrays.asList(fields));
        return rxe;
    }


    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RXE parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXE")) {
            throw new IllegalMessageFormat("Provided line does not start with RXE.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RXE RXE = new RXE(message);

        for (int i = 0; i < fields.length; i++) {
            RXE.setFieldValue(i, fields[i]);
        }

        return RXE;
    }
}
