package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class DG1 extends GenericSegment {

    public DG1(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public DG1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("DG1")) {
            throw new IllegalMessageFormat("Provided line does not start with DG1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        DG1 dg1 = new DG1(getMessage());
        dg1.getSegmentFields().addAll(Arrays.asList(fields));
        return dg1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static DG1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("DG1")) {
            throw new IllegalMessageFormat("Provided line does not start with DG1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        DG1 dg1 = new DG1(message);

        for (int i = 0; i < fields.length; i++) {
            dg1.setFieldValue(i, fields[i]);
        }

        return dg1;
    }
}
