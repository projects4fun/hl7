package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU5 extends GenericSegment {

    public ZU5(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU5 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU5")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU5.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU5 zu5 = new ZU5(getMessage());
        zu5.getSegmentFields().addAll(Arrays.asList(fields));
        return zu5;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU5 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU5")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU5.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU5 ZU5 = new ZU5(message);

        for (int i = 0; i < fields.length; i++) {
            ZU5.setFieldValue(i, fields[i]);
        }

        return ZU5;
    }
}
