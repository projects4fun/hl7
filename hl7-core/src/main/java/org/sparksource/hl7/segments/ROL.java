package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ROL extends GenericSegment {

    public ROL(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ROL build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ROL")) {
            throw new IllegalMessageFormat("Provided line does not start with ROL.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ROL rol = new ROL(getMessage());
        rol.getSegmentFields().addAll(Arrays.asList(fields));
        return rol;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ROL parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ROL")) {
            throw new IllegalMessageFormat("Provided line does not start with ROL.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ROL ROL = new ROL(message);

        for (int i = 0; i < fields.length; i++) {
            ROL.setFieldValue(i, fields[i]);
        }

        return ROL;
    }
}
