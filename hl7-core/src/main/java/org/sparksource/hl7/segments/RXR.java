package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RXR extends GenericSegment implements FieldAccessor<RXR, RXR.FieldName> {

    public enum FieldName {
        ROUTE(1),
        ADMINISTRATION_SITE(2),
        ADMINISTRATION_DEVICE(3),
        ADMINISTRATION_METHOD(4),
        ROUTING_INSTRUCTION(5),
        ADMINISTRATION_SITE_MODIFIER(6);

        final private int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public RXR(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RXR build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXR")) {
            throw new IllegalMessageFormat("Provided line does not start with RXR.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RXR rxr = new RXR(getMessage());
        rxr.getSegmentFields().addAll(Arrays.asList(fields));
        return rxr;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RXR parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RXR")) {
            throw new IllegalMessageFormat("Provided line does not start with RXR.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RXR RXR = new RXR(message);

        for (int i = 0; i < fields.length; i++) {
            RXR.setFieldValue(i, fields[i]);
        }

        return RXR;
    }

    /**
     * <p>Allows for values to be defined based on the declared enums of a
     * segment.</p>
     *
     * @param fieldName the field name that should be assigned
     * @param value     the value to be assigned
     * @return the current message for message chains
     */
    @Override
    public RXR setFieldValue(FieldName fieldName, String value) {
        setFieldValue(fieldName.value, value);
        return this;
    }

    /**
     * <p>Allows for a query on a field value</p>
     *
     * @param fieldName the field name being queried
     * @return an optional of the queried content
     */
    @Override
    public Optional<String> getFieldValue(FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
