package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class AIG extends GenericSegment {

    public AIG(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public AIG build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AIG")) {
            throw new IllegalMessageFormat("Provided line does not start with AIG.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        AIG aig = new AIG(getMessage());
        aig.getSegmentFields().addAll(Arrays.asList(fields));
        return aig;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static AIG parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AIG")) {
            throw new IllegalMessageFormat("Provided line does not start with AIG.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        AIG aig = new AIG(message);

        for (int i = 0; i < fields.length; i++) {
            aig.setFieldValue(i, fields[i]);
        }

        return aig;
    }
}
