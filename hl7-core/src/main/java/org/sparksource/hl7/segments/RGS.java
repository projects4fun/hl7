package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RGS extends GenericSegment {

    public RGS(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RGS build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RGS")) {
            throw new IllegalMessageFormat("Provided line does not start with RGS.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RGS rgs = new RGS(getMessage());
        rgs.getSegmentFields().addAll(Arrays.asList(fields));
        return rgs;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RGS parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RGS")) {
            throw new IllegalMessageFormat("Provided line does not start with RGS.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RGS RGS = new RGS(message);

        for (int i = 0; i < fields.length; i++) {
            RGS.setFieldValue(i, fields[i]);
        }

        return RGS;
    }
}
