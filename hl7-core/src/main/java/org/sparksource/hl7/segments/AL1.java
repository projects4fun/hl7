package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class AL1 extends GenericSegment {

    public AL1(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public AL1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AL1")) {
            throw new IllegalMessageFormat("Provided line does not start with AL1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        AL1 al1 = new AL1(getMessage());
        al1.getSegmentFields().addAll(Arrays.asList(fields));
        return al1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static AL1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("AL1")) {
            throw new IllegalMessageFormat("Provided line does not start with AL1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        AL1 al1 = new AL1(message);

        for (int i = 0; i < fields.length; i++) {
            al1.setFieldValue(i, fields[i]);
        }

        return al1;
    }
}
