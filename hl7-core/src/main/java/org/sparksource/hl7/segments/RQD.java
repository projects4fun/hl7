package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class RQD extends GenericSegment {

    public RQD(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public RQD build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RQD")) {
            throw new IllegalMessageFormat("Provided line does not start with RQD.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        RQD rqd = new RQD(getMessage());
        rqd.getSegmentFields().addAll(Arrays.asList(fields));
        return rqd;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static RQD parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("RQD")) {
            throw new IllegalMessageFormat("Provided line does not start with RQD.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        RQD RQD = new RQD(message);

        for (int i = 0; i < fields.length; i++) {
            RQD.setFieldValue(i, fields[i]);
        }

        return RQD;
    }
}
