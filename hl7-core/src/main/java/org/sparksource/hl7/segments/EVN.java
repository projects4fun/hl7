package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Optional;

/**
 * <p>An API of for building and interacting {@link EVN} segment.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-12
 */
public class EVN extends GenericSegment implements FieldAccessor<EVN, EVN.FieldName> {

    public enum FieldName {
        EVENT_TYPE_CODE(1), RECORDED_DATETIME(2),
        DATETIME_PLANNED_EVENT(3), EVENT_REASON_CODE(4),
        OPERATOR_ID(5), EVENT_OCCURRED(6);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public EVN(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public EVN build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("EVN")) {
            throw new IllegalMessageFormat("Provided line does not start with EVN.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            setFieldValue(i, fields[i]);
        }

        return this;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static EVN parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("EVN")) {
            throw new IllegalMessageFormat("Provided line does not start with EVN.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        EVN evn = new EVN(message);

        for (int i = 0; i < fields.length; i++) {
            evn.setFieldValue(i, fields[i]);
        }

        return evn;
    }

    public String getEventTypeCode() {
        return getFieldValue(FieldName.EVENT_TYPE_CODE).orElse("");
    }

    public void setEventTypeCode(String eventTypeCode) {
        setFieldValue(FieldName.EVENT_TYPE_CODE.getValue(), eventTypeCode);
    }

    public String getRecordedDateTime() {
        return getFieldValue(FieldName.RECORDED_DATETIME).orElse("");
    }

    public void setRecordedDateTime(String recordedDateTime) {
        setFieldValue(FieldName.RECORDED_DATETIME.getValue(), recordedDateTime);
    }

    public String getDateTimePlannedEvent() {
        return getFieldValue(FieldName.DATETIME_PLANNED_EVENT).orElse("");
    }

    public void setDateTimePlannedEvent(String dateTimePlannedEvent) {
        setFieldValue(FieldName.DATETIME_PLANNED_EVENT.getValue(), dateTimePlannedEvent);
    }

    public String getEventReasonCode() {
        return getFieldValue(FieldName.EVENT_REASON_CODE).orElse("");
    }

    public void setEventReasonCode(String eventReasonCode) {
        setFieldValue(FieldName.EVENT_REASON_CODE.getValue(), eventReasonCode);
    }

    public String getOperatorId() {
        return getFieldValue(FieldName.OPERATOR_ID).orElse("");
    }

    public void setOperatorId(String operatorId) {
        setFieldValue(FieldName.OPERATOR_ID.getValue(), operatorId);
    }

    public String getEventOccurred() {
        return getFieldValue(FieldName.EVENT_OCCURRED).orElse("");
    }

    public void setEventOccurred(String eventOccurred) {
        setFieldValue(FieldName.EVENT_OCCURRED.getValue(), eventOccurred);
    }

    /**
     * <p>Provides a means to define {@link String} values for a particular
     * field location.</p>
     *
     * @param fieldName the location a value belongs in
     * @param value     the content to be placed in a defined segment
     * @return this object for chaining sets
     */
    @Override
    public EVN setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value, value);
        return this;
    }

    /**
     * <p>Provides a means to query for a particular {@link FieldName}.</p>
     *
     * @param fieldName the value to be retrieve
     * @return an {@link Optional} of the content identified
     */
    @Override
    public Optional<String> getFieldValue(FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
