package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Optional;

/**
 * <p>Provides an API of sorts for building and interacting {@link MSA} segment in a HL7 hl7message. Additionally
 * provides getter and setter method that should be usable for performing transformations on a given {@link MSA} field.
 * If all else fails one should be able to use the {@link java.util.List} for direct manipulation via the {@link String}
 * items.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-16
 */
public class MSA extends GenericSegment implements FieldAccessor<MSA, MSA.FieldName> {

    public enum FieldName {
        /**
         * <p>This field contains an acknowledgment code, see message processing
         * rules. Refer to HL7 Table 0008 - Acknowledgment Code for valid
         * values.</p>
         */
        RESPONSE(1),
        /**
         * <p>This field contains the message control ID of the message sent by
         * the sending system. It allows the sending system to associate this
         * response with the message for which it is intended.</p>
         */
        MESSAGE_CONTROL_ID(2),
        /**
         * <p>The MSA-3 was deprecated as of v 2.4 and the detail was withdrawn
         * and removed from the standard as of v 2.7. The reader is referred to
         * the ERR segment. The ERR segment allows for richer descriptions of
         * the erroneous conditions.</p>
         */
        TEXT_MESSAGE(3),
        /**
         * <p>This optional numeric field is used in the sequence number
         * protocol.</p>
         */
        EXPECTED_SEQ_NUMBER(4),
        /**
         * <p>The MSA-5 was deprecated as of v2.2 and the detail was withdrawn
         * and removed from the standard as of v 2.5.</p>
         */
        DELAYED_ACK_TYPE(5),
        /**
         * <p>The MSA-3 was deprecated as of v 2.4 and the detail was withdrawn
         * and removed from the standard as of v 2.7. The reader is referred to
         * the ERR segment. The ERR segment allows for richer descriptions of
         * the erroneous conditions.</p>
         */
        ERROR_CONDITION(6);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public MSA(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public MSA build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("MSA")) {
            throw new IllegalMessageFormat("Provided line does not start with MSA.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));

        for (int i = 0; i < fields.length; i++) {
            setFieldValue(i, fields[i]);
        }

        return this;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static MSA parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("MSA")) {
            throw new IllegalMessageFormat("Provided line does not start with MSA.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        MSA MSA = new MSA(message);

        for (int i = 0; i < fields.length; i++) {
            MSA.setFieldValue(i, fields[i]);
        }

        return MSA;
    }

    public String getResponse() {
        return getFieldValue(FieldName.RESPONSE).orElse("");
    }

    public void setResponse(String response) {
        setFieldValue(FieldName.RESPONSE.value, response);
    }

    public String getMessageControlId() {
        return getFieldValue(FieldName.MESSAGE_CONTROL_ID).orElse("");
    }

    public void setMessageControlId(String messageControlId) {
        setFieldValue(FieldName.MESSAGE_CONTROL_ID.value, messageControlId);
    }

    /**
     * <p>Provides a means to define {@link String} values for a particular
     * field location.</p>
     *
     * @param fieldName the location a value belongs in
     * @param value     the content to be placed in a defined segment
     * @return this object for chaining sets
     */
    @Override
    public MSA setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value(), value);
        return this;
    }

    /**
     * <p>Provides a means to query for a particular {@link FieldName}.</p>
     *
     * @param fieldName the value to be retrieve
     * @return an {@link Optional} of the content identified
     */
    @Override
    public Optional<String> getFieldValue(FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
