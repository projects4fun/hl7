package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Juan Garcia
 * @since 2016-04-16
 */
public class ORC extends GenericSegment implements FieldAccessor<ORC, ORC.FieldName> {

    public enum FieldName {
        ORDER_CONTROL(1),
        PLACER_ORDER_NUMBER(2),
        FILLER_ORDER_NUMBER(3),
        PLACER_GROUP_NUMBER(4),
        ORDER_STATUS(5),
        RESPONSE_FLAG(6),
        QUANTITY_TIMING(7),
        PARENT_ORDER(8),
        DATETIME_OF_TRANSACTION(9),
        ENTERED_BY(10),
        VERIFIED_BY(11),
        ORDERING_PROVIDER(12),
        ENTERERS_LOCATION(13),
        CALL_BACK_PHONE_NUMBER(14),
        ORDER_EFFECTIVE_DATETIME(15),
        ORDER_CONTROL_CODE_REASON(16),
        ENTERING_ORGANIZATION(17),
        ENTERING_DEVICE(18),
        ACTION_BY(19),
        ADVANCED_BENEFICIARY_NOTICE_CODE(20),
        ORDERING_FACILITY_NAME(21),
        ORDERING_FACILITY_ADDRESS(22),
        ORDERING_FACILITY_PHONE_NUMBER(23),
        ORDERING_PROVIDER_ADDRESS(24),
        ORDER_STATUS_MODIFIER(25),
        ADVANCED_BENEFICIARY_NOTICE_OVERRIDE_REASON(26),
        FILLERS_EXPECTED_AVAILABILITY_DATE_TIME(27),
        CONFIDENTIALITY_CODE(28),
        ORDER_TYPE(29),
        ENTERER_AUTHORIZATION_MODE(30),
        PARENT_UNIVERSAL_SERVICE_IDENTIFIER(31);

        final private int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public ORC(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ORC build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ORC")) {
            throw new IllegalMessageFormat("Provided line does not start with ORC.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ORC orc = new ORC(getMessage());
        orc.getSegmentFields().addAll(Arrays.asList(fields));
        return orc;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ORC parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ORC")) {
            throw new IllegalMessageFormat("Provided line does not start with ORC.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ORC ORC = new ORC(message);

        for (int i = 0; i < fields.length; i++) {
            ORC.setFieldValue(i, fields[i]);
        }

        return ORC;
    }

    /**
     * <p>Allows for values to be defined based on the declared enums of a
     * segment.</p>
     *
     * @param fieldName the field name that should be assigned
     * @param value     the value to be assigned
     * @return the current message for message chains
     */
    @Override
    public ORC setFieldValue(ORC.FieldName fieldName, String value) {
        setFieldValue(fieldName.value, value);
        return this;
    }

    /**
     * <p>Allows for a query on a field value</p>
     *
     * @param fieldName the field name being queried
     * @return an optional of the queried content
     */
    @Override
    public Optional<String> getFieldValue(ORC.FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value) {
            return Optional.of(getSegmentFields().get(fieldName.value));
        } else {
            return Optional.empty();
        }
    }
}
