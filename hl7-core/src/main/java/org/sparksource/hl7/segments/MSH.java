package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;
import org.sparksource.hl7.generators.MessageControlIdGenerator;

import java.util.Optional;
import java.util.StringJoiner;

/**
 * <p>An API for interacting with the {@link MSH} segment.</p>
 *
 * @author Juan Garcia
 * @since 2016-04-11
 */
public class MSH extends GenericSegment implements FieldAccessor<MSH, MSH.FieldName> {

    /**
     * <p>Note that these items are not off set according to HL7 terms. In HL7 | would be the first
     * field of the MSH segment. In this application | is consumed as the delimiter and MSH becomes
     * the first item. For everything else standard Java Array conventions should apply - counting
     * starts 0.</p>
     */
    public enum FieldName {
        ENCODING_CHARACTERS(2), SENDING_APPLICATION(3),
        SENDING_FACILITY(4), RECEIVING_APPLICATION(5),
        RECEIVING_FACILITY(6), DATE_TIME(7),
        SECURITY(8), MESSAGE_TYPE(9),
        MESSAGE_CONTROL_ID(10), PROCESSING_ID(11),
        VERSION_ID(12), SEQUENCE_NUMBER(13),
        CONTINUATION_POINTER(14), ACCEPT_ACK_TYPE(15),
        APPLICATION_ACK_TYPE(16), COUNTRY_CODE(17),
        CHARACTER_SET(18), PRINCIPAL_LANG_OF_MESSAGE(19),
        ALT_CHAR_SET(20), MESSAGE_PROFILE_IDENTIFIER(21),
        SENDING_RESPONSIBLE_ORG(22), RECEIVING_RESPONSIBLE_ORG(23);

        private final int value;

        FieldName(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    // default delimiters to be used for new HL7 messages
    private String fieldDelimiter = "|";
    private String componentDelimiter = "^";
    private String subComponentDelimiter = "&";
    private String repeatDelimiter = "~";
    private String escapeCharacter = "\\";

    private String encodingCharacters;
    private String sendingApplication;
    private String sendingFacility;
    private String receivingApplication;
    private String receivingFacility;
    private String dateTime;
    private String security;
    private String messageType;
    private String messageControlId;
    private String processingId;
    private String versionId;
    private String sequenceNumber;
    private String continuationPointer;
    private String acceptAckType;
    private String applicationAckType;
    private String countryCode;
    private String characterSet;
    private String principalLangOfMessage;

    public MSH(final GenericMessage message) {
        super(message);
        setSegmentName(getClass().getSimpleName());
        getSegmentFields().addAll(getSegmentFields()); // TODO what?

        getMessage().setEscapeSequence(getMessage());
    }

    /**
     * <p>Creates a {@link MSH} from a {@link String}.</p>
     *
     * @param line the line to be transformed to a {@link MSH}
     * @return the {@link MSH} representation of a {@link String}
     * @throws IllegalMessageFormat if the line is an empty; if the line does not start with MSH
     */
    public MSH build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("MSH")) {
            throw new IllegalMessageFormat("Provided line does not start with MSH.");
        }

        setFieldDelimiter(String.valueOf(line.charAt(3)));
        setComponentDelimiter(String.valueOf(line.charAt(4)));
        setRepeatDelimiter(String.valueOf(line.charAt(5)));
        setEscapeCharacter(String.valueOf(line.charAt(6)));
        setSubComponentDelimiter(String.valueOf(line.charAt(7)));

        // we now know enough to configure the escape sequences
        getMessage().setEscapeSequence(getMessage());

        String[] strings = line.split(escapeDelimiter(getFieldDelimiter()));

        MSH msh = new MSH(getMessage());
        msh.setSegmentName(getClass().getSimpleName());
        msh.setFieldDelimiter(getFieldDelimiter());
        msh.setEncodingCharacters(strings, FieldName.ENCODING_CHARACTERS.value() - 1);
        msh.setSendingApplication(strings, FieldName.SENDING_APPLICATION.value() - 1);
        msh.setSendingFacility(strings, FieldName.SENDING_FACILITY.value() - 1);
        msh.setReceivingApplication(strings, FieldName.RECEIVING_APPLICATION.value() - 1);
        msh.setReceivingFacility(strings, FieldName.RECEIVING_FACILITY.value() - 1);
        msh.setDateTime(strings, FieldName.DATE_TIME.value() - 1);
        msh.setSecurity(strings, FieldName.SECURITY.value() - 1);
        msh.setMessageType(strings, FieldName.MESSAGE_TYPE.value() - 1);
        msh.setMessageControlId(strings, FieldName.MESSAGE_CONTROL_ID.value() - 1);
        msh.setProcessingId(strings, FieldName.PROCESSING_ID.value() - 1);
        msh.setVersionId(strings, FieldName.VERSION_ID.value() - 1);
        msh.setSequenceNumber(strings, FieldName.SEQUENCE_NUMBER.value() - 1);
        msh.setContinuationPointer(strings, FieldName.CONTINUATION_POINTER.value() - 1);
        msh.setAcceptAckType(strings, FieldName.ACCEPT_ACK_TYPE.value() - 1);
        msh.setApplicationAckType(strings, FieldName.APPLICATION_ACK_TYPE.value() - 1);
        msh.setCountryCode(strings, FieldName.COUNTRY_CODE.value() - 1);
        msh.setCharacterSet(strings, FieldName.CHARACTER_SET.value() - 1);
        msh.setPrincipalLangOfMessage(strings, FieldName.PRINCIPAL_LANG_OF_MESSAGE.value() - 1);

        return msh;
    }

    public static MSH parse(final String line) throws IllegalMessageFormat {
        return new MSH(new GenericMessage()).build(line);
    }

    public String getFieldDelimiter() {
        return fieldDelimiter;
    }

    public void setFieldDelimiter(String fieldDelimiter) {
        this.fieldDelimiter = fieldDelimiter;
    }

    public String getComponentDelimiter() {
        return componentDelimiter;
    }

    public void setComponentDelimiter(String componentDelimiter) {
        this.componentDelimiter = componentDelimiter;
    }

    public String getSubComponentDelimiter() {
        return subComponentDelimiter;
    }

    public void setSubComponentDelimiter(String subComponentDelimiter) {
        this.subComponentDelimiter = subComponentDelimiter;
    }

    public String getRepeatDelimiter() {
        return repeatDelimiter;
    }

    public void setRepeatDelimiter(String repeatDelimiter) {
        this.repeatDelimiter = repeatDelimiter;
    }

    public String getEscapeCharacter() {
        return escapeCharacter;
    }

    public void setEscapeCharacter(String escapeCharacter) {
        this.escapeCharacter = escapeCharacter;
    }

    public String getEncodingCharacters() {
        return encodingCharacters;
    }

    private void setEncodingCharacters(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setEncodingCharacters(fields[sequence]);
        }
    }

    public void setEncodingCharacters(String encodingCharacters) {
        this.encodingCharacters = encodingCharacters;
        setFieldValue(FieldName.ENCODING_CHARACTERS.value(), encodingCharacters);
    }

    public String getSendingApplication() {
        return sendingApplication;
    }

    private void setSendingApplication(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setSendingApplication(fields[sequence]);
        }
    }

    public void setSendingApplication(String sendingApplication) {
        this.sendingApplication = sendingApplication;
        setFieldValue(FieldName.SENDING_APPLICATION.value(), sendingApplication);
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    private void setSendingFacility(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setSendingFacility(fields[sequence]);
        }
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
        setFieldValue(FieldName.SENDING_FACILITY.value(), sendingFacility);
    }

    public String getReceivingApplication() {
        return receivingApplication;
    }

    private void setReceivingApplication(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setReceivingApplication(fields[sequence]);
        }
    }

    public void setReceivingApplication(String receivingApplication) {
        this.receivingApplication = receivingApplication;
        setFieldValue(FieldName.RECEIVING_APPLICATION.value(), receivingApplication);
    }

    public String getReceivingFacility() {
        return receivingFacility;
    }

    private void setReceivingFacility(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setReceivingFacility(fields[sequence]);
        }
    }

    public void setReceivingFacility(String receivingFacility) {
        this.receivingFacility = receivingFacility;
        setFieldValue(FieldName.RECEIVING_FACILITY.value(), receivingFacility);
    }

    public String getDateTime() {
        return dateTime;
    }

    private void setDateTime(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setDateTime(fields[sequence]);
        }
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
        setFieldValue(FieldName.DATE_TIME.value(), dateTime);
    }

    public String getSecurity() {
        return security;
    }

    private void setSecurity(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setSecurity(fields[sequence]);
        }
    }

    public void setSecurity(String security) {
        this.security = security;
        setFieldValue(FieldName.SECURITY.value(), security);
    }

    public String getMessageType() {
        return messageType;
    }

    private void setMessageType(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setMessageType(fields[sequence]);
        }
    }

    public void setMessageType(final String messageType) {
        this.messageType = messageType;
        setFieldValue(FieldName.MESSAGE_TYPE.value(), messageType);
    }

    public String getMessageControlId() {
        return messageControlId;
    }

    private void setMessageControlId(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setMessageControlId(fields[sequence]);
        }
    }

    /**
     * <p>Sets the HL7 message control id of the users choice.</p>
     *
     * @param messageControlId the value to be used as the message control id
     */
    public void setMessageControlId(final String messageControlId) {
        this.messageControlId = messageControlId;
        setFieldValue(FieldName.MESSAGE_CONTROL_ID.value(), messageControlId);
    }

    /**
     * <p>Sets the HL7 message control id leveraging {@link MessageControlIdGenerator}.</p>
     */
    public void setMessageControlId() {
        final String controlId = MessageControlIdGenerator.next();
        this.messageControlId = controlId;
        setFieldValue(FieldName.MESSAGE_CONTROL_ID.value(), controlId);
    }

    public String getProcessingId() {
        return processingId;
    }

    private void setProcessingId(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setProcessingId(fields[sequence]);
        }
    }

    public void setProcessingId(String processingId) {
        this.processingId = processingId;
        setFieldValue(FieldName.PROCESSING_ID.value(), processingId);
    }

    public String getVersionId() {
        return versionId;
    }

    private void setVersionId(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setVersionId(fields[sequence]);
        }
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
        setFieldValue(FieldName.VERSION_ID.value(), versionId);
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    private void setSequenceNumber(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setSequenceNumber(fields[sequence]);
        }
    }

    /**
     * <p>Defines the value that will be used as the MSH sequence item.</p>
     *
     * @param sequenceValue the {@link String} value to be used as a sequence
     */
    public void setSequenceNumber(final String sequenceValue) {
        this.sequenceNumber = sequenceValue;
        setFieldValue(FieldName.SEQUENCE_NUMBER.value(), sequenceValue);
    }

    /**
     * <p>The internal {@link MessageControlIdGenerator} creates a long value to assign as a
     * sequence number. This overloaded setter takes a long value as an
     * option.</p>
     *
     * @param sequenceValue the {@link Long} value to be used as a sequence
     */
    public void setSequenceNumber(final long sequenceValue) {
        this.sequenceNumber = Long.toString(sequenceValue);
        setFieldValue(FieldName.SEQUENCE_NUMBER.value(), sequenceNumber);
    }

    public String getContinuationPointer() {
        return continuationPointer;
    }

    private void setContinuationPointer(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setContinuationPointer(fields[sequence]);
        }
    }

    public void setContinuationPointer(String continuationPointer) {
        this.continuationPointer = continuationPointer;
        setFieldValue(FieldName.CONTINUATION_POINTER.value(), continuationPointer);
    }

    public String getAcceptAckType() {
        return acceptAckType;
    }

    private void setAcceptAckType(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setAcceptAckType(fields[sequence]);
        }
    }

    public void setAcceptAckType(String acceptAckType) {
        this.acceptAckType = acceptAckType;
        setFieldValue(FieldName.ACCEPT_ACK_TYPE.value(), acceptAckType);
    }

    public String getApplicationAckType() {
        return applicationAckType;
    }

    private void setApplicationAckType(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setApplicationAckType(fields[sequence]);
        }
    }

    public void setApplicationAckType(String applicationAckType) {
        this.applicationAckType = applicationAckType;
        setFieldValue(FieldName.APPLICATION_ACK_TYPE.value(), applicationAckType);
    }

    public String getCountryCode() {
        return countryCode;
    }

    private void setCountryCode(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setCountryCode(fields[sequence]);
        }
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        setFieldValue(FieldName.COUNTRY_CODE.value(), countryCode);
    }

    public String getCharacterSet() {
        return characterSet;
    }

    private void setCharacterSet(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setCharacterSet(fields[sequence]);
        }
    }

    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
        setFieldValue(FieldName.CHARACTER_SET.value(), characterSet);
    }

    public String getPrincipalLangOfMessage() {
        return principalLangOfMessage;
    }


    private void setPrincipalLangOfMessage(String[] fields, int sequence) {
        if (inBounds(fields, sequence)) {
            setPrincipalLangOfMessage(fields[sequence]);
        }
    }

    public void setPrincipalLangOfMessage(String principalLangOfMessage) {
        this.principalLangOfMessage = principalLangOfMessage;
        setFieldValue(FieldName.PRINCIPAL_LANG_OF_MESSAGE.value(), principalLangOfMessage);
    }
    //

    /**
     * @param fieldName
     * @param value
     * @return
     */
    @Override
    public MSH setFieldValue(final FieldName fieldName, final String value) {
        setFieldValue(fieldName.value(), value);
        return this;
    }

    /**
     * @param fieldName
     * @return
     */
    @Override
    public Optional<String> getFieldValue(final FieldName fieldName) {
        if (getSegmentFields().size() >= fieldName.value()) {
            return Optional.of(getSegmentFields().get(fieldName.value()));
        } else {
            return Optional.empty();
        }
    }

    // ========================================
    // ========================================

    @Override
    public String toString() {
        final String delimiter;
        if (getMessage() == null) {
            delimiter = DEFAULT_FIELD_DELIMITER;
        } else {
            delimiter = getMessage().getFieldDelimiter();
        }

        StringJoiner stringJoiner = new StringJoiner(delimiter);
        for (int i = 0; i < getSegmentFields().size(); i++) {
            if (i == 1) {
                continue;
            }

            stringJoiner.add(getSegmentFields().get(i) == null ? "" : getSegmentFields().get(i));
        }

        return stringJoiner.toString();
    }
}
