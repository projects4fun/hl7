package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ACC extends GenericSegment {

    public ACC(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ACC build(String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ACC")) {
            throw new IllegalMessageFormat("Provided line does not start with ACC.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ACC acc = new ACC(getMessage());
        acc.getSegmentFields().addAll(Arrays.asList(fields));
        return acc;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ACC parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ACC")) {
            throw new IllegalMessageFormat("Provided line does not start with ACC.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ACC acc = new ACC(message);

        for (int i = 0; i < fields.length; i++) {
            acc.setFieldValue(i, fields[i]);
        }

        return acc;
    }
}
