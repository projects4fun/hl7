package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU6 extends GenericSegment {

    public ZU6(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU6 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU6")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU6.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU6 zu6 = new ZU6(getMessage());
        zu6.getSegmentFields().addAll(Arrays.asList(fields));
        return zu6;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU6 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU6")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU6.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU6 ZU6 = new ZU6(message);

        for (int i = 0; i < fields.length; i++) {
            ZU6.setFieldValue(i, fields[i]);
        }

        return ZU6;
    }
}
