package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZNT extends GenericSegment {

    public ZNT(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZNT build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZNT")) {
            throw new IllegalMessageFormat("Provided line does not start with ZNT.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZNT znt = new ZNT(getMessage());
        znt.getSegmentFields().addAll(Arrays.asList(fields));
        return znt;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZNT parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZNT")) {
            throw new IllegalMessageFormat("Provided line does not start with ZNT.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZNT ZNT = new ZNT(message);

        for (int i = 0; i < fields.length; i++) {
            ZNT.setFieldValue(i, fields[i]);
        }

        return ZNT;
    }
}
