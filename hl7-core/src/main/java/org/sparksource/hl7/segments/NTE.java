package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-16
 */
public class NTE extends GenericSegment {

    public NTE(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public NTE build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("NTE")) {
            throw new IllegalMessageFormat("Provided line does not start with NTE.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        NTE nte = new NTE(getMessage());
        nte.getSegmentFields().addAll(Arrays.asList(fields));
        return nte;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static NTE parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("NTE")) {
            throw new IllegalMessageFormat("Provided line does not start with NTE.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        NTE NTE = new NTE(message);

        for (int i = 0; i < fields.length; i++) {
            NTE.setFieldValue(i, fields[i]);
        }

        return NTE;
    }
}
