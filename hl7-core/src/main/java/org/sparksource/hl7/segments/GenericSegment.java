package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * <p>A catch all {@link Segment} to ensure that a type we have not coded is not
 * discarded.</p>
 *
 * @author Juan Garcia
 * @since 2017-08-30
 */
public class GenericSegment extends Segment {

    final public static String DEFAULT_FIELD_DELIMITER = "|";
    final private static String[] REGEX_PROTECTED_CHARACTERS =
        {"\\", "^", "$", ".", "|", "?", "*", "+", "(", ")", "[", "{"};

    private String segmentName;

    /**
     * <p>Constructor with the {@link GenericMessage} in context. The {@link GenericMessage}
     * context is needed to instruct the remainder of the {@link GenericSegment} of
     * preferred delimiters.</p>
     *
     * @param message the {@link GenericMessage} that the {@link GenericSegment} belongs to
     */
    public GenericSegment(final GenericMessage message) {
        super(message);
    }

    /**
     * <p>Base {@link GenericSegment} requirement towards the construction of
     * messages.</p>
     *
     * @param line the value to be transformed to a typed object
     * @return a object of base type {@link GenericSegment}
     * @throws IllegalMessageFormat when a segment fails to be created
     */
    public GenericSegment build(String line) throws IllegalMessageFormat {
        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        GenericSegment segment = new GenericSegment(getMessage());
        segment.getSegmentFields().addAll(Arrays.asList(fields));
        return segment;
    }

    /**
     * <p>Internal method to help determine if a {@link GenericSegment} needs to be
     * extended to fit a given value.</p>
     *
     * @param array the array of {@link String} values to be compared
     * @param index the location where a value should be placed
     * @return true if the index is within the array boundary
     */
    protected static boolean inBounds(final String[] array, final int index) {
        return (index >= 0) && (index < array.length);
    }

    /**
     * <p>HL7 allows a system to define their HL7 control characters outside of
     * recommended items. While not typically seen support exists for ensuring
     * that the characters can be used as delimiter.</p>
     *
     * @param boundary the {@link String} to be used to separate fields
     * @return the proper delimiter that can be used internally
     */
    protected static String escapeDelimiter(String boundary) {
        for (final String string : REGEX_PROTECTED_CHARACTERS) {
            if (boundary.contains(string)) {
                boundary = boundary.replace(string, "\\" + string);
            }
        }
        return boundary.replace("\"", "");
    }

    /**
     * <p>An alternative method to {@link ArrayList#set(int, Object)} it checks
     * to ensure that the {@link ArrayList} is large enough to place the field.
     * When the {@link ArrayList} is not large enough we expand the {@link List}
     * to permit setting the values.</p>
     *
     * @param position location where the {@link String} value should be set.
     * @param value    the {@link String} of the content to place in a position
     */
    public void setFieldValue(final int position, String value) {
        if (getSegmentFields().size() < (position + 1)) {
            expandSegmentFields(position);
        }

        getSegmentFields().set(position, value);
    }

    /**
     * <p>Expands the list representing out {@link GenericSegment}. This method
     * is leveraged internally to permit defining items that may be a part of a
     * {@link GenericSegment} but not populated yet.</p>
     *
     * @param position the location to expend fields to
     */
    private void expandSegmentFields(final int position) {
        while (getSegmentFields().size() <= position) {
            getSegmentFields().add("");
        }
    }

    // ====================
    // ====================

    /**
     * <p>Provides the name for this {@link GenericSegment}.</p>
     *
     * @return the first three characters presenting the name of this segment
     */
    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(final String segmentName) {
        this.segmentName = segmentName;
        setFieldValue(0, segmentName);
    }

    // ====================
    // ====================

    @Override
    public String toString() {
        final String delimiter;
        if (getMessage() == null) {
            delimiter = DEFAULT_FIELD_DELIMITER;
        } else {
            delimiter = getMessage().getFieldDelimiter();
        }

        StringJoiner stringJoiner = new StringJoiner(delimiter);
        for (String s : getSegmentFields()) {
            stringJoiner.add(s == null ? "" : s);
        }

        return stringJoiner.toString();
    }
}
