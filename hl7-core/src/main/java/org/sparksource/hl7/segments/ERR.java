package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ERR extends GenericSegment {

    public ERR(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ERR build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ERR")) {
            throw new IllegalMessageFormat("Provided line does not start with ERR.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ERR err = new ERR(getMessage());
        err.getSegmentFields().addAll(Arrays.asList(fields));
        return err;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ERR parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ERR")) {
            throw new IllegalMessageFormat("Provided line does not start with ERR.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ERR err = new ERR(message);

        for (int i = 0; i < fields.length; i++) {
            err.setFieldValue(i, fields[i]);
        }

        return err;
    }
}
