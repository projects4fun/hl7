package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class PDA extends GenericSegment {

    public PDA(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public PDA build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PDA")) {
            throw new IllegalMessageFormat("Provided line does not start with PDA.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        PDA pda = new PDA(getMessage());
        pda.getSegmentFields().addAll(Arrays.asList(fields));
        return pda;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static PDA parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PDA")) {
            throw new IllegalMessageFormat("Provided line does not start with PDA.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        PDA PDA = new PDA(message);

        for (int i = 0; i < fields.length; i++) {
            PDA.setFieldValue(i, fields[i]);
        }

        return PDA;
    }
}
