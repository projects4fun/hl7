package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZU7 extends GenericSegment {

    public ZU7(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZU7 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU7")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU7.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZU7 zu7 = new ZU7(getMessage());
        zu7.getSegmentFields().addAll(Arrays.asList(fields));
        return zu7;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZU7 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZU7")) {
            throw new IllegalMessageFormat("Provided line does not start with ZU7.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZU7 ZU7 = new ZU7(message);

        for (int i = 0; i < fields.length; i++) {
            ZU7.setFieldValue(i, fields[i]);
        }

        return ZU7;
    }
}
