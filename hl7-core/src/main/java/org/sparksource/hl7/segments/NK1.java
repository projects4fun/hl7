package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class NK1 extends GenericSegment {

    public NK1(final GenericMessage message) {
        super(message);
        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public NK1 build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("NK1")) {
            throw new IllegalMessageFormat("Provided line does not start with NK1.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        NK1 nk1 = new NK1(getMessage());
        nk1.getSegmentFields().addAll(Arrays.asList(fields));
        return nk1;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static NK1 parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("NK1")) {
            throw new IllegalMessageFormat("Provided line does not start with NK1.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        NK1 NK1 = new NK1(message);

        for (int i = 0; i < fields.length; i++) {
            NK1.setFieldValue(i, fields[i]);
        }

        return NK1;
    }
}
