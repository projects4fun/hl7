package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class PRB extends GenericSegment {

    public PRB(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public PRB build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PRB")) {
            throw new IllegalMessageFormat("Provided line does not start with PRB.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        PRB prb = new PRB(getMessage());
        prb.getSegmentFields().addAll(Arrays.asList(fields));
        return prb;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static PRB parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("PRB")) {
            throw new IllegalMessageFormat("Provided line does not start with PRB.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        PRB PRB = new PRB(message);

        for (int i = 0; i < fields.length; i++) {
            PRB.setFieldValue(i, fields[i]);
        }

        return PRB;
    }
}
