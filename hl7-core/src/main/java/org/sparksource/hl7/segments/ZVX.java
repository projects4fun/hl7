package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class ZVX extends GenericSegment {

    public ZVX(final GenericMessage message) {
        super(message);

        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public ZVX build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZVX")) {
            throw new IllegalMessageFormat("Provided line does not start with ZVX.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        ZVX zvx = new ZVX(getMessage());
        zvx.getSegmentFields().addAll(Arrays.asList(fields));
        return zvx;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static ZVX parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("ZVX")) {
            throw new IllegalMessageFormat("Provided line does not start with ZVX.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        ZVX ZVX = new ZVX(message);

        for (int i = 0; i < fields.length; i++) {
            ZVX.setFieldValue(i, fields[i]);
        }

        return ZVX;
    }
}
