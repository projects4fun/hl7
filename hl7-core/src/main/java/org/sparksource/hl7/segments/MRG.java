package org.sparksource.hl7.segments;

import org.sparksource.hl7.GenericMessage;
import org.sparksource.hl7.IllegalMessageFormat;

import java.util.Arrays;

/**
 * @author Juan Garcia
 * @since 2016-04-17
 */
public class MRG extends GenericSegment {

    public MRG(final GenericMessage message) {
        super(message);
        setSegmentName(getClass().getSimpleName());
    }

    @Override
    public MRG build(final String line) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("MRG")) {
            throw new IllegalMessageFormat("Provided line does not start with MRG.");
        }

        String[] fields = line.split(escapeDelimiter(getMessage().getFieldDelimiter()));
        MRG mrg = new MRG(getMessage());
        mrg.getSegmentFields().addAll(Arrays.asList(fields));
        return mrg;
    }

    /**
     * <p>Static creation for reflection based creation</p>
     *
     * @param line    the line to be parsed
     * @param message the message to determine how to parse
     * @return an object of this class type
     * @throws IllegalMessageFormat if the segment fails to be built
     */
    public static MRG parse(final String line, GenericMessage message) throws IllegalMessageFormat {
        if (line == null || line.trim().equals("") || !line.startsWith("MRG")) {
            throw new IllegalMessageFormat("Provided line does not start with MRG.");
        }

        String[] fields = line.split(escapeDelimiter(message.getFieldDelimiter()));

        MRG MRG = new MRG(message);

        for (int i = 0; i < fields.length; i++) {
            MRG.setFieldValue(i, fields[i]);
        }

        return MRG;
    }
}
